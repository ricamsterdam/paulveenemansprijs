<#
.SYNOPSIS
  Builds and packages a solution using MSBuild and Inno Setup Compiler.

.DESCRIPTION
  This script uses VSWhere to locate the latest installed version of MSBuild, builds a
  solution file, and creates an installer using Inno Setup Compiler.

.EXAMPLE
  .\build-and-package.ps1
#>
#Requires -Version 5

function Find-MSBuildPath {
  $InstallationPath = & "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -latest -products * -requires Microsoft.Component.MSBuild -Property installationPath
  $MSBuildPath = Join-Path -Path $InstallationPath -ChildPath "MSBuild\Current\Bin\MSBuild.exe"

  if (Test-Path -Path $MSBuildPath) {
    return $MSBuildPath
  }

  Write-Host -ForegroundColor Red -Object "ERROR: MSBuild not found."
  return $null
}

function Build-Solution ([string]$MSBuildPath,[string]$SolutionPath) {
  & $MSBuildPath /t:Clean,Rebuild /p:Configuration=Release $SolutionPath

  if ($LASTEXITCODE -eq 0) {
    return $true
  }

  Write-Host -ForegroundColor Red -Object "ERROR: Build failed."
  return $false
}

function Find-InnoSetupCompiler {
  $IsccPath = ${env:ProgramFiles(x86)},$env:ProgramFiles |
  ForEach-Object {
    # Look only into directories that start with "Inno"
    Get-ChildItem -Path $_ -Filter "Inno*" -Directory |
    ForEach-Object {
      (Get-ChildItem -Path $_.FullName -Filter "ISCC.exe" -File -Recurse).FullName
    }
  } |
  Select-Object -First 1

  if ($null -ne $IsccPath -and (Test-Path -Path $IsccPath)) {
    return $IsccPath
  }

  Write-Host -ForegroundColor Red -Object "ERROR: Inno Setup Compiler not found."
  return $null
}

function Get-AppNameFromIss ([string]$IssFilePath) {
  $issContent = Get-Content $IssFilePath -Raw

  if ($issContent -match '^#define appname ["'']?([0-9A-Za-z]+)') {
    return $Matches[1]
  }

  return $null
}

function New-PackageInstaller ([string]$IsccPath,[string]$IssFilePath) {
  $appName = Get-AppNameFromIss $IssFilePath
  $SetupFilePattern = "setup-$appName-*.exe"

  Remove-Item -Path $SetupFilePattern -ErrorAction SilentlyContinue

  & $IsccPath $IssFilePath | Out-Null

  if ($LASTEXITCODE -ne 0) {
    Write-Host -ForegroundColor Red "ERROR: Inno Setup compilation failed."
    return $null
  }

  return $SetupFilePattern
}

function Wait-Return {
  if ($Host.Name -ne "ConsoleHost") {
    return
  }

  Write-Host -ForegroundColor Yellow -Object "Press any key to exit..."
  $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

# Main

if ($host.Version.Major -ge 7) { $PSStyle.Progress.View = "Minimal" } elseif ($Host.Name -eq "ConsoleHost") { Write-Host -Object "`n`n`n`n`n`n`n" }
$MyInvocationPath = Split-Path -Parent $MyInvocation.MyCommand.Path
Push-Location $MyInvocationPath

$MSBuildPath = Find-MSBuildPath
$IsccPath = Find-InnoSetupCompiler
$CreateAndPushPath = Join-Path -Path $MyInvocationPath -ChildPath "create-and-push.ps1"

if (-not $MSBuildPath -or -not $IsccPath) {
  return Wait-Return
}

$Solution = Get-ChildItem -Filter *.sln

if (-not $Solution) {
  Write-Host -ForegroundColor Red "ERROR: Solution not found."
  return Wait-Return
}

Write-Progress -Activity "Build and Package" -Status "Building Solution..." -PercentComplete 25
if (-not (Build-Solution -MSBuildPath $MSBuildPath -SolutionPath $Solution.FullName)) {
  return Wait-Return
}

$percentComplete = 65
Get-ChildItem -Filter *.iss |
ForEach-Object {
  Write-Progress -Activity "Build and Package" -Status "Building Installer '$($_.Name)'..." -PercentComplete $percentComplete
  $percentComplete += 5
  $SetupFilePattern = New-PackageInstaller -IsccPath $IsccPath -IssFilePath $_.FullName
  if ($SetupFilePattern) {
    Write-Progress -Activity "Build and Package" -Status "Creating NuGet Package '$($_.Name)'..." -PercentComplete $percentComplete
    $percentComplete += 5
    & $CreateAndPushPath -SetupFilePattern $SetupFilePattern
  }
}

Pop-Location
Write-Progress -Activity "Build and Package" -Status "Completed" -PercentComplete 99
Start-Sleep -Seconds 1
Write-Progress -Activity "Build and Package" -Completed
Write-Host -ForegroundColor Green -Object "Done."
Wait-Return
