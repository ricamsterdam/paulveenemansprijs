#define appname "PaulVeenemansPrijsApp"
#define fileversion GetFileVersion(AddBackslash(SourcePath) + appname + "\bin\"+ appname + ".dll")
#define shortversion Copy(fileversion, 1, RPos(".", Copy(fileversion, 1, RPos(".", fileversion) - 1)) - 1)

[Setup]
AllowUNCPath=false
AppId={{8A83B4BD-D8CD-4416-956F-DE60729CD1FF}
AppName={#appname}
AppVersion={#appname} {#fileversion}
Compression=lzma2/ultra64
DefaultDirName=D:\inetpub\PaulVeenemansPrijsApp
DirExistsWarning=no
DisableProgramGroupPage=no
DisableReadyPage=true
LanguageDetectionMethod=none
OutputBaseFilename=setup-{#appname}-{#shortversion}
OutputDir=..
SetupLogging=true
ShowLanguageDialog=no
SolidCompression=true
SourceDir={#appname}
UninstallFilesDir={app}-uninstall
VersionInfoVersion={#fileversion}
UsePreviousAppDir=False

[Dirs]
Name: {app}-uninstall\DO_NOT_DELETE; BeforeInstall: MyBeforeInstall;
Name: {app}\App_Data;
Name: {app}\App_Data\WorkDirectory;

[Files]
Source: bin\*.dll; DestDir: {app}\bin;
Source: bin\roslyn\*.*; DestDir: {app}\bin\roslyn;
Source: Areas\Admin\Views\*.*; DestDir: {app}\Areas\Admin\Views; Flags: recursesubdirs;
Source: Content\*.*; DestDir: {app}\Content; Flags: recursesubdirs;
Source: favicon.ico; DestDir: {app};
Source: Global.asax; DestDir: {app};
Source: packages.config; DestDir: {app};
Source: Scripts\*.*; DestDir: {app}\Scripts; Flags: recursesubdirs
Source: Views\*.*; DestDir: {app}\Views; Flags: recursesubdirs
Source: Web.config; DestDir: {app};

[UninstallDelete]
Type: files; name: "{app}\*.log"
Type: dirifempty; name: "{app}"

[Code]
procedure MyBeforeInstall();
{ Uninstall previous version first, so redundant old files will be properly removed. }
var
  UninstDir: String;
  FindRec: TFindRec;
  ResultCode: Integer;
begin
  UninstDir := ExpandConstant('{app}-uninstall\');
  if FindFirst(UninstDir + 'unins*.exe', FindRec) then
    Exec(UninstDir + FindRec.name, '/VERYSILENT', '', SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode);
end;

