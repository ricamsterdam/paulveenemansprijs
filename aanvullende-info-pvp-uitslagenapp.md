Aanvullende informatie PVP-uitslagenapplicatie
==============================================

Inleiding
---------

De PVP-uitslagenapplicatie is een hulpmiddel voor het berekenen van de uitslagen bij de Paul Veenemans Prijs en de publicatie van die uitslagen. De applicatie bevat geen functies voor de tijdwaarneming, die dient apart plaats te vinden. De tijden worden per onderdeel geïmporteerd in de applicatie.

Tijdwaarneming
---------------

Tijdwaarneming bij de Paul Veenemans Prijs gebeurt over het algemeen op de volgende manieren:
- *hardlopen*, *wielrennen* en *schaatsen*: via het ProChip systeem van [MYLAPS](https://www.mylaps.com); dit wordt gehuurd, inclusief de assistentie door een MYLAPS-medewerker.
- *roeien*: door één van de gespecialiseerde organisaties voor de tijdwaarneming van roeiwedstrijden: [IRIS](https://poweredbyiris.nl), [Time-Team](https://time-team.nl) of [Roeitijden.nl](https://www.roeitijden.nl). Het is ook prima mogelijk om de tijdwaarneming zelf te doen via bijvoorbeeld [RaceSplitter](https://www.racesplitter.com) of zelfs gewone ouderwetse gesynchroniseerde stopwatches. Dit hangt af van het budget en van de ervaring van de wedstrijdorganisatie met tijdwaarneming van roeiwedstrijden.

Voorbereidingen
---------------

1.  Na sluiting van de wijzigingstermijn: inschrijvingen downloaden uit
    inschrijvingen.knrb.nl

2.  Excel-bestand maken met in elk geval de volgende kolommen:

    1.  A (Leeglaten, hier komen de startnummers)

    2.  B Chipcode van MyLaps

    3.  C Veld

    4.  D Indien gebruikt: subveld (=veteranen-categorie)

    5.  E Vereniging + volgnummer (samen teamcode)

    6.  F Deelnemer (in de vorm: voornaam tussenvoegsel achternaam)

    7.  G Code "2-" voor roeien of "1x" voor skiffen

    8.  H Racegroep (dit is de samenhangende groep van roeiers waarover
        één klassement berekend wordt)

3.  Bestand kan worden uitgebreid met extra kolommen voor de
    verschillende onderdelen, of andere toevoegingen over betaling e.d.
    Voor de import in de tijdverwerkingsapplicatie mogen echter alleen
    bovenstaande acht kolommen gebruikt worden. Even een kopietje maken
    dus.

4.  In de eerste kolom (A) moeten nog startnummers worden ingevuld. Zie
    hieronder.

5.  Indelen/plaatsen/loten volgens vooraf opgestelde regels.

    -   Over het algemeen worden velden niet door elkaar geloot; met
        subvelden wordt geen rekening gehouden. Die dus wel door elkaar.

    -   Gebruikelijk is verder om eerst alle deelnemers in de tweezonder
        een startnummer te geven volgens de finishvolgorde van vorig
        jaar, maar in elk geval zo dat ploeggenoten opeenvolgende
        startnummers krijgen. (Bij het roeien heeft een tweezonder twee
        startnummers; je voorkomt veel verwarring als deze nummers
        altijd opeenvolgend zijn.)

    -   Voorts wordt bij het plaatsen van deelnemers de uitslagvolgorde
        van vorig jaar aangehouden.

    -   Deelnemers die het vorige jaar niet hebben deelgenomen worden
        geloot.

    -   Het is gebruikelijk om een paar startnummers ruimte te laten
        tussen elk veld zodat late na-inschrijvers kunnen worden
        tussengevoegd. Maar noodzakelijk is dit niet echt; eventueel kun
        je ook afwijkende startnummers toekennen.

    -   Het resultaat van het lotingsproces is dat het startnummer de
        startvolgorde bij het roeien weergeeft. Voor andere onderdelen
        kunnen eventueel andere volgordes worden aangehouden. (Vooral
        bij het schaatsen worden nog wel eens blokken gevormd waarbij de
        startnummers niet strict opeenvolgend zijn.)

6.  Dit indelen/plaatsen/loten is een handmatig proces. De
    uitslagenberekeningsapplicatie helpt hier niet bij. Het lijkt veel
    werk, maar over het algemeen is het toch een overzichtelijk klusje.
    En een eventueel verkeerd toegekend startnummer is niet direct
    rampzalig. Drie onderdelen worden met chiptijden geregistreerd, daar
    is het startnummer van ondergeschikt belang. Het vierde onderdeel is
    roeien, en daar hebben we allemaal veel ervaring mee, ook als er
    ergens een afwijkend nummer gebruikt wordt. Maar het is natuurlijk
    beter om alles netjes op orde te hebben.

7.  De chip met de juiste code en het juiste startnummer gaan in een
    enveloppe met de naam van de deelnemer erop. Chip, startnummer en
    naam deelnemer mogen natuurlijk niet verwisseld worden.

8.  Het Excel-bestand dat je onder punt 2 en 3 gemaakt hebt kun je
    alvast importeren in de uitslagenapplicatie. Het is op zich handig
    om dit op vrijdag te doen. Niet te lang van tevoren want misschien
    verandert er nog iets. Niet te kort van tevoren want waarom zou je
    tijdsdruk creëren als het niet nodig is.

9.  Alles is klaar voor de uitslagenverwerking. Ga verder met de
    instructies uit het hulpbestand.

+++

Uitslagen weergeven via de eigen PVP-site
-----------------------------------------

Opties voor weergave van de uitslagenpagina:

1.  Inframen met een iframe. De pagina is 980 pixels breed. De hoogte is
    variabel. Simpelste oplossing is om een flinke extra ruimte te nemen
    aan de onderkant, mooier is om een kleine Javascript te gebruiken om
    de hoogte in te stellen.

2.  Losse link naar de uitslagenpagina op
    https://2017.paulveenemansprijs.nl/. Op deze manier staat er geen
    extra content rond de pagina. Dit is het gebruikersvriendelijkste op
    een smartphone. (Als je de telefoon liggend vasthoudt, dan is alles
    vrij goed leesbaar.)

3.  HTML ophalen via een API. In overleg kan ik ook een API-endpoint
    maken waarmee jullie de HTML (of JSON) server-side ophalen en
    volledig integreren binnen pvp2017.nl. Prachtige oplossing, maar
    vrij veel werk aan jullie kant.

De styling van de huidige uitslagenpagina kan naar behoefte worden
aangepast.

Veel Voorkomende Vragen
-----------------------

*Vraag:* Ik mis de variable 'verenigingsnaam' of 'verenigingscode' in de
export. Kan deze toegevoegd worden?

*Antwoord:* Mocht je wel een export vanuit de PVP-applicatie willen
gebruiken, dan kun je de export vanuit "voorlopige scores" nemen. Daar
staat in kolom E de ploegnaam, waar de verenigingsnaam ook in zit.

*Vraag:* Hoe kan ik in de export zien welke startnummers bij elkaar in
de 2- zitten?

*Antwoord:* In de PVP-applicatie wordt niet bijgehouden wie er bij
elkaar in de tweezonder zitten. Deze gegevens dienen natuurlijk wel
opgenomen te worden op de startlijst voor het roeien. Als het goed is
zal de wedstrijdleiding zodanig loten dat tweezonder-maatjes
opeenvolgende startnummers hebben. De exacte gegevens ontvang je van de
wedstrijdorganisatie.

*Vraag:* Hoe kan ik aan de export zien welk startnummer deelneemt aan
welke disciplines?

*Antwoord:* In de PVP-applicatie zelf wordt vooraf niet bijgehouden
welke deelnemers meedoen aan welke disciplines. Dat is voor de
uitslagen-applicatie ook niet van belang. De wedstrijdleiding weet dit
natuurlijk wel en zal jullie van de juiste startlijsten moeten voorzien.
(Voor het roeien is het gemakkelijk; daar doet iedereen aan mee. Voor
het hardlopen krijgen jullie van hen een lijst met startende
deelnemers.) Bedenk dat de PVP-applicatie alleen een systeem is voor de
uitslagenverwerking, niet voor het compleet managen van de wedstrijd.

*Vraag:* Welke encoding wordt gebruikt in het .xls formaat? Welke
encoding dienen wij te gebruiken?

*Antwoord:* TL;DR: dat kun je beter niet zelf managen. Lang antwoord: de
PVP-applicatie maakt gebruik van het binaire XLS-formaat. Technisch
gesproken is dit BIFF, waarbij de encoding van text strings te vinden is
in de metadata van het werkboek of werkblad (namelijk in het CODEPAGE
record). Maar ik hoop van harte dat jullie gebruik maken van een library
om Excel-bestanden te lezen en te schrijven; het BIFF-formaat is
buitengewoon complex en het correct parsen van een BIFF-file valt echt
niet mee. Voor alle programmeertalen die ook maar iets voorstellen
bestaan standaard-library's voor het lezen en schrijven van Excel.

XLSX gebruikt intern Unicode; in de package zitten XML-files die hun
eigen encoding definiëren. (Ook hier kun je beter een library gebruiken
om te parsen en te schrijven.)

*Vraag:* Is alleen het xls formaat toegestaan, of kunnen wij ook xlsx of
csv aanleveren?

*Antwoord:* XLSX wordt volledig ondersteund. CSV wordt niet ondersteund.
De reden dat ik CSV niet ondersteun is dat het geen gestandaardiseerd
formaat is. Maar je kunt natuurlijk eenvoudig een CSV-bestand openen
in Excel en het wegschrijven als XLS. In Excel voer je meteen een
visuele controle uit of de conversie van CSV naar Excel geslaagd is.

*Vraag:* De separator voor het tijdgedeelte dat kleiner is dan een
seconde (tienden/honderdsten/miliseconden) wordt in de PVP applicatie
inconsequent gebruikt (punt of komma), welke variant dienen wij aan te
leveren?

*Antwoord:* De formattering van de tijdsinterval is onbelangrijk. In
Excel worden tijden opgeslagen als getallen. Eén dag is gelijk aan één
gehele tijdseenheid. Een uur is 1/24 = 0.041666667, een minuut is 1/1440
= 0.000694444, een seconde is 1/86400 = 0.0000115740740740741 en een
honderdste seconde is 1/8640000 = 0.000000115740740740741. (Je mag deze
waarden formatteren zoals je wilt in Excel, de import-routine gebruikt
alleen de onderliggende waarde.)

*Vraag:* Standaard rekenen wij met tijden met drie decimalen, maar geven
wij tijden weer in honderdsten. Ik zie dat er in de applicatie gewerkt
wordt met tienden. Wat moet ik aanleveren? Wanneer wij het aantal
decimalen verminderen, moeten wij dan afronden, of afkappen?

*Antwoord:* Afronden gebeurt in de PVP-applicatie. Lever de tijden dus
zo ruw mogelijk aan. Ik kan me niet voorstellen dat je bij een tijdrace
op duizendsten nauwkeurig kunt klokken, maar als je duizendsten hebt dan
mag je ze aanleveren. Traditioneel wordt een tijdrace geklokt in
honderdsten (want dat is wat een stopwatch weergeeft) en afgerond op
tienden, maar de PVP-applicatie slaat intern duizendsten van seconden op
(zoals veel tijdwaarnemeningsapplicaties trouwens). Bij boord-aan-boord
is het trouwens weer een ander verhaal.

*Vraag:* Zoals u wellicht weet is tijdmeting niet altijd een exacte
bezigheid. Wij houden daarom bij het bepalen van de ranking aan dat als
tijden binnen 0.1 seconde van elkaar liggen, ze dezelfde rank moeten
krijgen.

*Antwoord:* In het algemeen: dat is een interessante benadering. Zoals
je misschien weet zegt het RVR niets over de manier waarop de
tijdwaarneming dient plaats te vinden. Er wordt stilzwijgend van een
oneindige nauwkeurigheid uitgegaan. (Dit vind ik al jaren een ernstige
omissie, maar naar mij luisteren ze natuurlijk niet.)

Maar: in het reglement voor de PVP staat wél dat gerekend moet worden
met een nauwkeurigheid van eentiende van een seconde. (Sterker nog: er
staat dat eerst moet worden afgerond op tienden van een seconde, en dat
pas daarna de punten moeten worden toegekend. Maakt allemaal niet uit,
de PVP-applicatie houdt zich exact aan de voorschriften van het
PVP-reglement, dus jullie hoeven je daar verder niet in te verdiepen.)
Er staat ook in het PVP-reglement hoe er gehandeld moet worden bij ex
aequo-resultaten: zie artikel 9. (En ook dat handelt de PVP-applicatie
correct af. Ook dat is dus niet van belang voor het importeren van de
Excel-sheets.)

Om een lang verhaal kort te maken: jullie hoeven alleen de ruwe tijden
aan te leveren. Ranking, afronding, score-berekening en uitslagbepaling
gebeurt in de PVP-applicatie. Die is daar speciaal voor gemaakt.

Bekende issues met de applicatie
--------------------------------

De manier om straffen toe te kennen is nog steeds niet duidelijk genoeg. Er is een dialoogvenster dat het probeert uit te leggen, maar het is nog steeds onduidelijk wat je nou eigenlijk moet doen.

+++

Toelichting voorbeeld-uitslagen roeien
--------------------------------------

Ik heb een voorbeelddocument bijgevoegd van een fictieve roei-uitslag.
In het voorbeeld roeiden startnummer 1 en 2 samen in een tweezonder, en
3 en 4 alsook 5 en 6. De rest roeide in een skiff. Meer dan twee
kolommen heeft de PVP-applicatie niet nodig. (Eén kolom voor startnummer
of chipnummers en één kolom voor de gerealiseerde tijdsperiodes) Als je
wilt zien hoe Excel intern de tijdsinterval heeft opgeslagen, dan kun je
de weergave van kolom H op "General" zetten. (Met: Format Cells =\>
Number =\> General)
