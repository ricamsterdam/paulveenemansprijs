<#
.SYNOPSIS
   Create Chocolatey NuGet packages for a set of Inno Setup installer files.

.DESCRIPTION
  This script generates Chocolatey NuGet packages for a set of Inno Setup installer
  files. It creates the required NuSpec file, the tool folder and the ChocolateyInstall.ps1
  script for each installer. After packaging, it pushes the generated NuGet packages to a
  predefined NuGet repository.

.PARAMETER SetupFilePattern
   Specifies the pattern for the Inno Setup installer files. Default is "setup-*.exe".

.EXAMPLE
   .\create-and-push.ps1 -SetupFilePattern "mysetup-*.exe"
#>
#Requires -Version 5

param(
  [string]$SetupFilePattern = "setup-*.exe"
)

$ErrorActionPreference = "Stop"

# Check if nuget and choco commands are available
Get-Command -Name nuget | Out-Null
Get-Command -Name choco | Out-Null

function New-NuSpecFile ([string]$PackageName,[string]$Version) {
  $nuSpecContent = @"
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
  <metadata>
    <id>$PackageName</id>
    <version>$Version</version>
    <authors>st2x.net</authors>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>$PackageName</description>
  </metadata>
  <files>
    <file src="tools\**" target="tools" />
  </files>
</package>
"@
  $nuSpecContent | Out-File -FilePath "$PackageName.nuspec" -Encoding UTF8
}

function New-TemporaryDirectoryAndSetWD ([string]$Path) {
  if (Test-Path -Path $Path) {
    Remove-Item -Path $Path -Force -Recurse
  }
  New-Item -Path $Path -ItemType Directory | Out-Null
  Push-Location -Path $Path
}

function New-ToolsDirectoryAndCopyInstaller ([string]$SetupFilePath,[string]$SetupFileName) {
  New-Item -Path tools -ItemType directory | Out-Null
  Copy-Item -Path $SetupFilePath -Destination tools
  New-Item -Path "tools\$SetupFileName.ignore" -ItemType file | Out-Null
}

function New-ChocolateyInstallScript ([string]$PackageName,[string]$SetupFileName,[string]$Destination) {
  $chocoInstallScript = @"
`$packageName = "$PackageName"
`$fileType = "exe"
`$silentArgs = "/SILENT"
`$scriptPath =  `$(Split-Path `$MyInvocation.MyCommand.Path)
`$fileFullPath = Join-Path `$scriptPath "$SetupFileName"

try {
  Install-ChocolateyInstallPackage -PackageName `$packageName -FileType `$fileType -SilentArgs `$silentArgs -FileFullPath `$fileFullPath
} catch {
  Write-ChocolateyFailure -PackageName `$packageName -Message `$_.Exception.Message
  throw
}
"@
  $chocoInstallScript | Out-File -FilePath "$Destination\chocolateyInstall.ps1" -Encoding ASCII
}

function New-ChocolateyPackageFromHere {
  choco pack | Out-Null
  return (Get-Item -Path *.nupkg).Name
}

function Remove-TemporaryDirectory ([string]$Path) {
  Pop-Location
  Remove-Item -Path $Path -Force -Recurse
}

Get-Item -Path $SetupFilePattern |
ForEach-Object {
  $SetupFilePath = $_.FullName
  $SetupFileName = $_.Name

  if (-not (Test-Path -Path $SetupFilePath -PathType Leaf)) {
    throw [System.IO.FileNotFoundException]::new($SetupFilePath)
  }

  $PackageName = $SetupFileName -replace "^setup-","" -replace "-\d.*exe$",""
  $Version = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($SetupFilePath).FileVersion

  New-TemporaryDirectoryAndSetWD -Path temp
  New-NuSpecFile -PackageName $PackageName -Version $Version
  New-ToolsDirectoryAndCopyInstaller -SetupFilePath $SetupFilePath -SetupFileName $SetupFileName
  New-ChocolateyInstallScript -PackageName $PackageName -SetupFileName $SetupFileName -Destination tools
  $nuPkg = New-ChocolateyPackageFromHere
  Move-Item -Path $nuPkg -Destination .. -Force
  Remove-TemporaryDirectory -Path temp

  # Push the NuGet package to the repository
  & nuget push $nuPkg -ApiKey admin:${env:chocopass} -Source https://nuget.st2x.net/nuget/choco
  Remove-Item -Path $nuPkg
}
