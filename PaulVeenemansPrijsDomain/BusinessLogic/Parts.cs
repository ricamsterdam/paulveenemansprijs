namespace PaulVeenemansPrijsDomain.BusinessLogic
{
    public static class Parts
    {
        public static readonly string Skating = "Skating";
        public static readonly string Running = "Running";
        public static readonly string Cycling = "Cycling";
        public static readonly string Rowing = "Rowing";
    }
}