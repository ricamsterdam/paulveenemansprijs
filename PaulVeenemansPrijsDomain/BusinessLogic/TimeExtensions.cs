﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PaulVeenemansPrijsDomain.BusinessLogic
{
    public static class TimeExtensions
    {
        public static string FromIntToTime(this double milliseconds)
        {
            return ((int?)milliseconds).FromIntToTime();
        }

        public static string FromIntToTime(this int? milliseconds)
        {
            return milliseconds == null
                       ? ""
                       : string.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00.0}", milliseconds / 60000, (milliseconds % 60000) / 1000m);
        }

        public static DateTime? FromIntToDateTime(this int? milliseconds)
        {
            return milliseconds == null ? null as DateTime? : new DateTime(1899, 12, 31).Add(TimeSpan.FromMilliseconds((int)milliseconds));
        }

        public static int? FromTimeToInt(this string time)
        {
            if (time == null)
            {
                return null;
            }

            Match match = Regex.Match(time, @"^\s*(\d\d):(\d\d(?:\.\d+)?)\s*$");
            if (match.Success)
            {
                int milliseconds = int.Parse(match.Groups[1].Value) * 60000 + (int)(double.Parse(match.Groups[2].Value) * 1000);
                if (milliseconds != 0)
                {
                    return milliseconds;
                }
            }

            return null;
        }
    }
}
