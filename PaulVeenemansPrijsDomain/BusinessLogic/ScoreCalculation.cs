using System.Collections.Generic;
using System.Linq;
using NLog;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.BusinessLogic.ScoreCalculationHelpers;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;

namespace PaulVeenemansPrijsDomain.BusinessLogic
{
    public static class ScoreCalculation
    {
        public static Logger MyLogger = LogManager.GetCurrentClassLogger();

        public static string Calculate(string whatPart, int raceGroup, IParticipantRepository participantRepository)
        {
            string report = CalculatePart(whatPart, raceGroup, participantRepository);

            foreach (Participant participant in participantRepository.FilterBy(p => p.RaceGroup.RaceGroupId == raceGroup))
            {
                participant.OverallScore = participant.GetOverallScore();
            }
            return report;
        }

        public static void ArchiveOverallScores(IParticipantApprovedRepository participantApprovedRepository,
            IParticipantArchivedRepository participantArchivedRepository)
        {
            participantArchivedRepository.Delete(participantArchivedRepository.All());
            foreach (ParticipantApproved participantApproved in participantApprovedRepository.All())
            {
                var participantArchived = new ParticipantArchived();
                participantArchived.InjectFrom(participantApproved);
                participantArchivedRepository.Add(participantArchived);
            }
        }

        public static void PublishOverallScores(IParticipantRepository participantRepository, IParticipantApprovedRepository participantApprovedRepository)
        {
            participantApprovedRepository.Delete(participantApprovedRepository.All());
            foreach (Participant participant in participantRepository.All())
            {
                var participantApproved = new ParticipantApproved();
                participantApproved.InjectFrom(participant);
                participantApprovedRepository.Add(participantApproved);
            }
        }

        public static void UnArchiveOverallScores(IParticipantArchivedRepository participantArchivedRepository,
            IParticipantApprovedRepository participantApprovedRepository)
        {
            if (participantArchivedRepository.All().Any())
            {
                participantApprovedRepository.Delete(participantApprovedRepository.All());
                foreach (ParticipantArchived participantArchived in participantArchivedRepository.All())
                {
                    var participantApproved = new ParticipantApproved();
                    participantApproved.InjectFrom(participantArchived);
                    participantApprovedRepository.Add(participantApproved);
                }
            }
            else
            {
                foreach (ParticipantApproved participantApproved in participantApprovedRepository.All())
                {
                    participantApproved.RankMajor = null;
                    participantApproved.RankMinor = null;
                    participantApproved.Skating = new Part();
                    participantApproved.Running = new Part();
                    participantApproved.Cycling = new Part();
                    participantApproved.Rowing = new RowingPart();
                    participantApproved.OverallScore = 0;
                    participantApprovedRepository.Add(participantApproved);
                }
            }
        }

        public static void CalculateRankings(int raceGroup, IParticipantRepository participantRepository)
        {
            var previousRaceCode = new RaceCode();
            var i = 1;
            foreach (Participant participant in
                participantRepository.FilterBy(p => p.RaceGroup.RaceGroupId == raceGroup)
                    .OrderBy(p => p.RaceCodeMajor ?? "")
                    .ThenByDescending(p => p.OverallScore)
                    .ThenByDescending(p => p.Rowing.Score)
                    .ThenByDescending(p => p.Running.Score))
            {
                var raceCode = new RaceCode(participant.RaceCodeMajor, "");
                if (raceCode != previousRaceCode)
                {
                    i = 1;
                    previousRaceCode = raceCode;
                }
                participant.RankMajor = i++;
            }

            previousRaceCode = new RaceCode();
            i = 1;
            foreach (Participant participant in
                participantRepository.FilterBy(p => p.RaceGroup.RaceGroupId == raceGroup)
                    .OrderBy(p => p.RaceCodeMajor ?? "")
                    .ThenBy(p => p.RaceCodeMinor ?? "")
                    .ThenByDescending(p => p.OverallScore)
                    .ThenByDescending(p => p.Rowing.Score)
                    .ThenByDescending(p => p.Running.Score))
            {
                var raceCode = new RaceCode(participant.RaceCodeMajor, participant.RaceCodeMinor);
                if (raceCode != previousRaceCode)
                {
                    i = 1;
                    previousRaceCode = raceCode;
                }
                participant.RankMinor = i++;
            }
        }

        private static string CalculatePart(string part, int raceGroup, IParticipantRepository participantRepository)
        {
            ParticipantTime[] participants = participantRepository
                .FilterBy(p => p.RaceGroup.RaceGroupId == raceGroup)
                .Select(p => new ParticipantTime(p, part))
                .ToArray();

            IEnumerable<ParticipantTime> list = participants.Where(p => p.Time != 0 && p.Status.Code == Status.Def);

            var partCalculator = new PartCalculator(part, list);
            partCalculator.SetCount().SetFastest().SetMedian().Assign();

            IEnumerable<ParticipantTime> listToZero = participants.Where(p => p.Time == 0 || p.Status.Code != Status.Def);
            foreach (ParticipantTime pt in listToZero)
            {
                pt.Score = 0;
            }

            return partCalculator.Report;
        }

        private static double GetOverallScore(this Participant participant)
        {
            double minimum = new[] {participant.Skating.Score, participant.Running.Score, participant.Cycling.Score}.Min();
            return participant.Cycling.Score + participant.Rowing.Score + participant.Running.Score + participant.Skating.Score - minimum;
        }
    }
}