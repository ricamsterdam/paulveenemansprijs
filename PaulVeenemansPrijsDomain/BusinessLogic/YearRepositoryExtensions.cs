﻿using System.Linq;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;

namespace PaulVeenemansPrijsDomain.BusinessLogic
{
    public static class YearRepositoryExtensions
    {
        public static Year CurrentYear(this IYearRepository repository)
        {
            return repository.All().FirstOrDefault();
        }
    }
}