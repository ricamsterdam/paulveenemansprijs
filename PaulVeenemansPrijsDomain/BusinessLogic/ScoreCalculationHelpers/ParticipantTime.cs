using System;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.BusinessLogic.ScoreCalculationHelpers
{
    internal class ParticipantTime
    {
        private readonly Part _part;
        private readonly Participant _participant;

        public ParticipantTime(Participant participant, string partName)
        {
            _participant = participant;
            if (partName == Parts.Skating)
            {
                _part = participant.Skating;
            }
            else if (partName == Parts.Running)
            {
                _part = participant.Running;
            }
            else if (partName == Parts.Cycling)
            {
                _part = participant.Cycling;
            }
            else if (partName == Parts.Rowing)
            {
                _part = participant.Rowing;
            }

            var unroundedTime = partName == Parts.Rowing ? participant.Rowing.TimeCorrected : _part?.Time;
            Time = RoundMillisecondsToTenths(unroundedTime);
        }

        public string ParticipantName => _participant.ParticipantName;

        public double Score
        {
            get => _part.Score;
            set => _part.Score = value;
        }

        public Status Status => _part.Status;
        public int Time { get; }

        private static int RoundMillisecondsToTenths(int? time)
        {
            var t = time ?? 0;
            return (int)Math.Round(t / 100m) * 100;
        }
    }
}