using System;

namespace PaulVeenemansPrijsDomain.BusinessLogic.ScoreCalculationHelpers
{
    internal readonly struct RaceCode : IEquatable<RaceCode>
    {
        private readonly string _major;
        private readonly string _minor;

        public RaceCode(string major, string minor)
        {
            _major = major;
            _minor = minor;
        }

        public bool Equals(RaceCode other)
        {
            return string.Equals(_major ?? "", other._major ?? "") && string.Equals(_minor ?? "", other._minor ?? "");
        }

        public override bool Equals(object obj)
        {
            return obj is RaceCode code && Equals(code);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_major ?? "").GetHashCode() * 397) ^ (_minor ?? "").GetHashCode();
            }
        }

        public static bool operator ==(RaceCode left, RaceCode right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(RaceCode left, RaceCode right)
        {
            return !left.Equals(right);
        }
    }
}
