using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace PaulVeenemansPrijsDomain.BusinessLogic.ScoreCalculationHelpers
{
    internal class PartCalculator
    {
        public static Logger MyLogger = LogManager.GetCurrentClassLogger();
        private readonly ParticipantTime[] _list;
        private readonly string _partName;
        private readonly List<string> _report = new List<string> {$"Berekening {DateTime.Now:s}"};
        private int _count;
        private int _fastest;
        private bool _isCalculation = true;
        private double _median;

        public PartCalculator(string partName, IEnumerable<ParticipantTime> list)
        {
            _list = list.OrderBy(p => p.Time).ToArray();
            _partName = partName;
        }

        public string Report => string.Join("\r\n", _report);

        public void Assign()
        {
            if (!_isCalculation)
            {
                return;
            }
            if (_fastest == 0)
            {
                throw new Exception("SetFastest() first.");
            }
            if (Math.Abs(_median) < 0.001)
            {
                throw new Exception("SetMedian() first.");
            }

            foreach (ParticipantTime pt in _list)
            {
                pt.Score = 100 + 10 * (_median - pt.Time) / (_median - _fastest);
                if (pt.Score < 0)
                {
                    pt.Score = 0;
                }
            }
        }

        public PartCalculator SetCount()
        {
            _count = _list.Length;
            if (_count < 2)
            {
                _report.Add("Minder dan twee deelnemers. Geen berekening.");
                _isCalculation = false;
            }
            else
            {
                _report.Add($"{_count} deelnemers in groep.");
            }
            return this;
        }

        public PartCalculator SetFastest()
        {
            if (!_isCalculation)
            {
                return this;
            }

            _fastest = _list[0].Time;
            MyLogger.Trace($"{_partName}: Fastest {_list[0].ParticipantName} {((int?)_fastest).FromIntToTime()}");
            _report.Add($"Snelste \"{_list[0].ParticipantName}\" {((int?)_fastest).FromIntToTime()}");
            return this;
        }

        public PartCalculator SetMedian()
        {
            if (!_isCalculation)
            {
                return this;
            }
            if (_count == 0)
            {
                throw new Exception("SetCount() first.");
            }

            if (_count % 2 != 0)
            {
                ParticipantTime candidate = _list[_count / 2];
                _median = candidate.Time;

                string logInfo = $"{candidate.ParticipantName} {_median.FromIntToTime()}";
                MyLogger.Trace($"{_partName}: Odd number of participants, median {logInfo}");
                _report.Add($"Oneven aantal deelnemers, mediaan {logInfo}");
            }
            else
            {
                ParticipantTime candidate1 = _list[_count / 2 - 1];
                ParticipantTime candidate2 = _list[_count / 2];
                double time1 = candidate1.Time;
                double time2 = candidate2.Time;
                _median = (time1 + time2) / 2;

                string logInfo = string.Join(" ",
                    $"\"{candidate1.ParticipantName}\" {time1.FromIntToTime()},",
                    $"\"{candidate2.ParticipantName}\" {time2.FromIntToTime()}",
                    "=>",
                    $"{_median.FromIntToTime()}");
                MyLogger.Trace($"{_partName}: Even number of participants, median average {logInfo}");
                _report.Add("Even aantal deelnemers, mediaan via gemiddelde.");
                _report.Add(logInfo);
            }
            return this;
        }
    }
}
