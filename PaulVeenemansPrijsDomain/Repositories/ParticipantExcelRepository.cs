using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class ParticipantExcelRepository : RepositoryBase<ParticipantExcel>, IParticipantExcelRepository
    {
        public ParticipantExcelRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IParticipantExcelRepository : IRepository<ParticipantExcel> {}
}