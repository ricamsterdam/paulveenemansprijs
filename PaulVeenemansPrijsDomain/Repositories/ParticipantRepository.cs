using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class ParticipantRepository : RepositoryBase<Participant>, IParticipantRepository
    {
        public ParticipantRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IParticipantRepository : IRepository<Participant> {}
}