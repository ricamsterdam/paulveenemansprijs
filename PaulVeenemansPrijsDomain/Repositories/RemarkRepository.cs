﻿using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class RemarkRepository : RepositoryBase<Remark>, IRemarkRepository
    {
        public RemarkRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IRemarkRepository : IRepository<Remark> {}
}