﻿using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class RaceGroupRepository : RepositoryBase<RaceGroup>, IRaceGroupRepository
    {
        public RaceGroupRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IRaceGroupRepository : IRepository<RaceGroup> {}
}