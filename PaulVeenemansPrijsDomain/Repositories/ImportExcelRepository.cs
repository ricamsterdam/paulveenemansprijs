using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class ImportExcelRepository : RepositoryBase<ImportExcel>, IImportExcelRepository
    {
        public ImportExcelRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IImportExcelRepository : IRepository<ImportExcel> {}
}