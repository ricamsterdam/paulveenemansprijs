﻿using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class CalculationReportRepository : RepositoryBase<CalculationReport>, ICalculationReportRepository
    {
        public CalculationReportRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) { }
    }

    public interface ICalculationReportRepository : IRepository<CalculationReport> { }
}
