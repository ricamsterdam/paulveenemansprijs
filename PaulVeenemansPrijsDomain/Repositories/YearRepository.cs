﻿using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class YearRepository : RepositoryBase<Year>, IYearRepository
    {
        public YearRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) { }
    }

    public interface IYearRepository : IRepository<Year> { }
}