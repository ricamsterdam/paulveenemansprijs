using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class ParticipantApprovedRepository : RepositoryBase<ParticipantApproved>, IParticipantApprovedRepository
    {
        public ParticipantApprovedRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IParticipantApprovedRepository : IRepository<ParticipantApproved> {}
}