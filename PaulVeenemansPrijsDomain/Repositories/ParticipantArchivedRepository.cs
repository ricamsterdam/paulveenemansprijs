using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class ParticipantArchivedRepository : RepositoryBase<ParticipantArchived>, IParticipantArchivedRepository
    {
        public ParticipantArchivedRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IParticipantArchivedRepository : IRepository<ParticipantArchived> {}
}