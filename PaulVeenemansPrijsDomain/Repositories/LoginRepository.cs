using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class LoginRepository : RepositoryBase<Login>, ILoginRepository
    {
        public LoginRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface ILoginRepository : IRepository<Login> {}
}