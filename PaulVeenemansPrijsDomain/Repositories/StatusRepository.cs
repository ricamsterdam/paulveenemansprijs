﻿using Mtk.Domain.NHibernate;
using Mtk.Domain.Repository;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.Repositories
{
    public class StatusRepository : ReadOnlyRepositoryBase<Status>, IStatusRepository
    {
        public StatusRepository(IMtkTransactionProvider transactionProvider) : base(transactionProvider) {}
    }

    public interface IStatusRepository : IReadOnlyRepository<Status> {}
}