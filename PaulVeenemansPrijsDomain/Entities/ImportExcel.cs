﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class ImportExcel
    {
        [Key]
        public virtual int ImportExcelId { get; set; }

        [Required]
        public virtual string Race { get; set; }

        [Display(Name = "ChipNr Column")]
        public virtual int? ChipNumberColumn { get; set; }

        [Display(Name = "TeamNr Column")]
        public virtual int? TeamNumberColumn { get; set; }

        [Required, Display(Name = "Time Column")]
        public virtual int TimeColumn { get; set; }

        [Required, Display(Name = "Skip Rows")]
        public virtual int SkipRows { get; set; }

        [Display(Name = "Upload Date")]
        public virtual DateTime? UploadDate { get; set; }

        [Display(Name = "Import Date")]
        public virtual DateTime? ImportDate { get; set; }

        [Display(Name = "Excel File Name")]
        public virtual string ExcelFileName { get; set; }

        public virtual byte[] ExcelData { get; set; }
    }
}