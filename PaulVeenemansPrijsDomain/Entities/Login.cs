﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class Login
    {
        [Required, Display(Name = "User name"), Key]
        public virtual string UserName { get; set; }

        [Required, DataType(DataType.Password), Display(Name = "Password")]
        public virtual string Password { get; set; }

        [Display(Name = "Remember me?")]
        public virtual bool RememberMe { get; set; }
    }
}