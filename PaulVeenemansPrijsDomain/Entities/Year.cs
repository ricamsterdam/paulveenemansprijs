﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class Year
    {
        [Required, Key]
        public virtual int YearId { get; set; }

        [Required, Display(Name = "Year")]
        public virtual string Name { get; set; }
    }
}