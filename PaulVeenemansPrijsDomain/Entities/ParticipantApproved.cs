﻿namespace PaulVeenemansPrijsDomain.Entities
{
    public class ParticipantApproved : IParticipant
    {
        public virtual int ParticipantId { get; set; }
        public virtual int TeamSeqNumber { get; set; }
        public virtual string ChipNumber { get; set; }
        public virtual string RaceCodeMajor { get; set; }
        public virtual string RaceCodeMinor { get; set; }
        public virtual string TeamCode { get; set; }
        public virtual string ParticipantName { get; set; }
        public virtual string CodeRowing { get; set; }
        public virtual Part Skating { get; set; }
        public virtual Part Running { get; set; }
        public virtual Part Cycling { get; set; }
        public virtual RowingPart Rowing { get; set; }
        public virtual double OverallScore { get; set; }
        public virtual int? RankMajor { get; set; }
        public virtual int? RankMinor { get; set; }
        public virtual RaceGroup RaceGroup { get; set; }
    }
}