﻿namespace PaulVeenemansPrijsDomain.Entities
{
    public class Remark
    {
        public virtual int RemarkId { get; set; }
        public virtual RaceGroup RaceGroup { get; set; }
        public virtual string RemarkText { get; set; }
    }
}