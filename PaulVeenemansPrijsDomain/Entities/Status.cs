﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class Status
    {
        public const string Def = "DEF";

        [Required, Key]
        public virtual int StatusId { get; set; }

        [Required, Display(Name = "Status Code")]
        public virtual string Code { get; set; }

        [Required]
        public virtual string Description { get; set; }
    }
}