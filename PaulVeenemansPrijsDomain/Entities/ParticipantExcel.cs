﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class ParticipantExcel
    {
        [Key]
        public virtual int ParticipantExcelId { get; set; }

        [Display(Name = "Excel File Name")]
        public virtual string ExcelFileName { get; set; }

        public virtual byte[] ExcelData { get; set; }
    }
}