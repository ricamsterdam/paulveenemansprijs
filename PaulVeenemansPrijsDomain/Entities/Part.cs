﻿namespace PaulVeenemansPrijsDomain.Entities
{
    /// <summary>
    ///     Component.
    /// </summary>
    public class Part
    {
        public virtual int? Time { get; set; }
        public virtual int? Penalty { get; set; }
        public virtual Status Status { get; set; }
        public virtual double Score { get; set; }
    }

    /// <inheritdoc />
    public class RowingPart : Part
    {
        public virtual int? TimeCorrected { get; set; }
    }
}