﻿namespace PaulVeenemansPrijsDomain.Entities
{
    public interface IParticipant {
        int ParticipantId { get; set; }
        int TeamSeqNumber { get; set; }
        string ChipNumber { get; set; }
        string RaceCodeMajor { get; set; }
        string RaceCodeMinor { get; set; }
        string TeamCode { get; set; }
        string ParticipantName { get; set; }
        string CodeRowing { get; set; }
        Part Skating { get; set; }
        Part Running { get; set; }
        Part Cycling { get; set; }
        RowingPart Rowing { get; set; }
        double OverallScore { get; set; }
        int? RankMajor { get; set; }
        int? RankMinor { get; set; }
        RaceGroup RaceGroup { get; set; }
    }
}