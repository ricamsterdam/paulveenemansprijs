﻿using System.Collections.Generic;

namespace PaulVeenemansPrijsDomain.Entities
{
    public class RaceGroup
    {
        public virtual int RaceGroupId { get; set; }
        public virtual string RaceGroupDescription { get; set; }
        public virtual IList<Participant> Participants { get; set; }
        public virtual IList<ParticipantApproved> ParticipantsApproved { get; set; }
        public virtual IList<Remark> Remarks { get; set; }
        public virtual IList<CalculationReport> CalculationReports { get; set; }
    }
}