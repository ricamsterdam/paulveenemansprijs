﻿namespace PaulVeenemansPrijsDomain.Entities
{
    public class CalculationReport
    {
        public virtual int CalculationReportId { get; set; }
        public virtual RaceGroup RaceGroup { get; set; }
        public virtual string Part { get; set; }
        public virtual string Description { get; set; }
    }
}
