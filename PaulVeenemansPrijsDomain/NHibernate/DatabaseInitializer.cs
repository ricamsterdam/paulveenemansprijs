﻿using System.Configuration;
using System.Reflection;
using FluentNHibernate.Cfg.Db;
using Mtk.Domain.NHibernate;

namespace PaulVeenemansPrijsDomain.NHibernate
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        public string ConnectionString => ConfigurationManager.ConnectionStrings["PaulVeenemansPrijsContext"].ConnectionString;

        #region IDatabaseInitializer Members
        public IPersistenceConfigurer Configuration => MsSqlConfiguration.MsSql2008.ConnectionString(ConnectionString);

        public Assembly DomainAssembly => Assembly.GetExecutingAssembly();
        #endregion
    }
}