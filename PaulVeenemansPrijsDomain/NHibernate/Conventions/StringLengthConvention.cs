using System.ComponentModel.DataAnnotations;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace PaulVeenemansPrijsDomain.NHibernate.Conventions
{
    public class StringLengthConvention : IPropertyConvention
    {
        #region IPropertyConvention Members
        public void Apply(IPropertyInstance instance)
        {
            if (!instance.Property.MemberInfo.IsDefined(typeof (StringLengthAttribute), false))
            {
                return;
            }
            object[] customAttributes = instance.Property.MemberInfo.GetCustomAttributes(typeof (StringLengthAttribute), false);
            var stringLengthAttribute = (StringLengthAttribute)(customAttributes[0]);
            instance.Length(stringLengthAttribute.MaximumLength);
        }
        #endregion
    }
}