using System;
using FluentNHibernate;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace PaulVeenemansPrijsDomain.NHibernate.Conventions
{
    public class ForeignKeyNameConvention : ForeignKeyConvention, IReferenceConvention, IHasManyConvention
    {
        #region IHasManyConvention Members
        public void Apply(IOneToManyCollectionInstance instance)
        {
            instance.Key.ForeignKey(string.Format("{0}_{1}Fk", instance.Relationship.Class.Name, instance.Relationship.EntityType.Name));
        }
        #endregion

        #region IReferenceConvention Members
        public new void Apply(IManyToOneInstance instance)
        {
            string columnName = GetKeyName(instance.Property, instance.Class.GetUnderlyingSystemType());
            instance.Column(columnName);
            instance.ForeignKey(string.Format("{0}_{1}Fk", instance.EntityType.Name, instance.Name));
        }
        #endregion

        protected override string GetKeyName(Member property, Type type)
        {
            return property != null ? property.Name : type.Name;
        }
    }
}