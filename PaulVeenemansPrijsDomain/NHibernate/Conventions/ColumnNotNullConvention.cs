using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace PaulVeenemansPrijsDomain.NHibernate.Conventions
{
    public class ColumnNotNullConvention : IPropertyConvention
    {
        #region IPropertyConvention Members
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Property.MemberInfo.IsDefined(typeof (RequiredAttribute), false))
            {
                instance.Not.Nullable();
            }
        }
        #endregion
    }

    public class IndexConvention : IPropertyConvention
    {
        #region IPropertyConvention Members
        public void Apply(IPropertyInstance instance)
        {
            if (Regex.IsMatch(instance.Name, @"(?:name|code|number)$", RegexOptions.IgnoreCase) && instance.Property.PropertyType != typeof (bool))
            {
                instance.Index(instance.Name);
            }
        }
        #endregion
    }
}