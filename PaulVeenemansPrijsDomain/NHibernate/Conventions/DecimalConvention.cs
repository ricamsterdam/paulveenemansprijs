using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace PaulVeenemansPrijsDomain.NHibernate.Conventions
{
    public class DecimalConvention : IPropertyConvention
    {
        #region IPropertyConvention Members
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Property.PropertyType != typeof (decimal))
            {
                return;
            }
            instance.Precision(10);
            instance.Scale(2);
        }
        #endregion
    }
}