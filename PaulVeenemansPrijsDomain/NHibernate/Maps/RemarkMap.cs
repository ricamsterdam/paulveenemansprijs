﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class RemarkMap : ClassMap<Remark>
    {
        public RemarkMap()
        {
            Id(x => x.RemarkId);
            References(x => x.RaceGroup);
            Map(x => x.RemarkText);
        }
    }
}