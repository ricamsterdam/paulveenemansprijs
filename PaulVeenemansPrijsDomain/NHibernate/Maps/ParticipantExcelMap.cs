﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class ParticipantExcelMap : ClassMap<ParticipantExcel>
    {
        public ParticipantExcelMap()
        {
            Id(x => x.ParticipantExcelId);
            Map(x => x.ExcelFileName);
            Map(x => x.ExcelData).Length(2147483647);
        }
    }
}