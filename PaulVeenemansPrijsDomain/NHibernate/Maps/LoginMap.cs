﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class LoginMap : ClassMap<Login>
    {
        public LoginMap()
        {
            Id(x => x.UserName);
            Map(x => x.Password);
            Map(x => x.RememberMe);
        }
    }
}