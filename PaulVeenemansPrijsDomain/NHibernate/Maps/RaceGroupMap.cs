﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class RaceGroupMap : ClassMap<RaceGroup>
    {
        public RaceGroupMap()
        {
            Id(x => x.RaceGroupId);
            Map(x => x.RaceGroupDescription);
            HasMany(x => x.Participants);
            HasMany(x => x.ParticipantsApproved);
            HasMany(x => x.Remarks);
            HasMany(x => x.CalculationReports);
        }
    }
}