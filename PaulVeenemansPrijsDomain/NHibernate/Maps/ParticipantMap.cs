﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class ParticipantMap : ClassMap<Participant>
    {
        public ParticipantMap()
        {
            Id(x => x.ParticipantId);
            Map(x => x.TeamSeqNumber);
            Map(x => x.ChipNumber);
            Map(x => x.RaceCodeMajor);
            Map(x => x.RaceCodeMinor);
            Map(x => x.TeamCode);
            Map(x => x.ParticipantName);
            Map(x => x.CodeRowing);
            Component(x => x.Skating, a =>
                {
                    a.Map(x => x.Time, "skating_time");
                    a.Map(x => x.Penalty, "skating_penalty");
                    a.References(x => x.Status, "skating_status_id");
                    a.Map(x => x.Score, "skating_score");
                });
            Component(x => x.Running, a =>
                {
                    a.Map(x => x.Time, "running_time");
                    a.Map(x => x.Penalty, "running_penalty");
                    a.References(x => x.Status, "running_status_id");
                    a.Map(x => x.Score, "running_score");
                });
            Component(x => x.Cycling, a =>
                {
                    a.Map(x => x.Time, "cycling_time");
                    a.Map(x => x.Penalty, "cycling_penalty");
                    a.References(x => x.Status, "cycling_status_id");
                    a.Map(x => x.Score, "cycling_score");
                });
            Component(x => x.Rowing, a =>
                {
                    a.Map(x => x.Time, "rowing_time");
                    a.Map(x => x.TimeCorrected, "rowing_time_corrected");
                    a.Map(x => x.Penalty, "rowing_penalty");
                    a.References(x => x.Status, "rowing_status_id");
                    a.Map(x => x.Score, "rowing_score");
                });
            Map(x => x.OverallScore);
            Map(x => x.RankMajor);
            Map(x => x.RankMinor);
            References(x => x.RaceGroup);
        }
    }
}