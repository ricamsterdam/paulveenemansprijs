﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps {
    internal sealed class CalculationReportMap : ClassMap<CalculationReport>
    {
        public CalculationReportMap()
        {
            Id(x => x.CalculationReportId);
            References(x => x.RaceGroup);
            Map(x => x.Part);
            Map(x => x.Description);
        }
    }
}