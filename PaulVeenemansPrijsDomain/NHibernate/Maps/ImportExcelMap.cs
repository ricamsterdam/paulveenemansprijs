﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class ImportExcelMap : ClassMap<ImportExcel>
    {
        public ImportExcelMap()
        {
            Id(x => x.ImportExcelId);
            Map(x => x.Race);
            Map(x => x.ChipNumberColumn);
            Map(x => x.TeamNumberColumn);
            Map(x => x.TimeColumn);
            Map(x => x.SkipRows);
            Map(x => x.UploadDate);
            Map(x => x.ImportDate);
            Map(x => x.ExcelFileName);
            Map(x => x.ExcelData).Length(2147483647);
        }
    }
}