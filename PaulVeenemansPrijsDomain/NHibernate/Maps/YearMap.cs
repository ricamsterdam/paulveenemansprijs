﻿using FluentNHibernate.Mapping;
using PaulVeenemansPrijsDomain.Entities;

namespace PaulVeenemansPrijsDomain.NHibernate.Maps
{
    internal sealed class YearMap : ClassMap<Year>
    {
        public YearMap()
        {
            Id(x => x.YearId);
            Map(x => x.Name);
        }
    }
}