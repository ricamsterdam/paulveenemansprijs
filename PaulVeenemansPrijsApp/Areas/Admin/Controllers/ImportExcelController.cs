using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.Pagination;
using Mtk.Site.Mvc.StatusMessage;
using Mtk.Site.Mvc.UI.Grid;
using Mtk.Site.Utility;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.BusinessLogic;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize]
    public class ImportExcelController : MtkController
    {
        private readonly ImportExcelRepository _importExcelRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly ICalculationReportRepository _calculationReportRepository;

        public ImportExcelController(ImportExcelRepository importExcelRepository, IParticipantRepository participantRepository,
            ICalculationReportRepository calculationReportRepository)
        {
            _importExcelRepository = importExcelRepository;
            _participantRepository = participantRepository;
            _calculationReportRepository = calculationReportRepository;
        }

        //
        // GET: /ImportExcel/

        [HttpGet]
        public ViewResult Index(GridSortOptions sort, int? page)
        {
            const int pageSize = 20;
            ViewBag.sort = sort;
            IQueryable<ImportExcel> queryable = _importExcelRepository.All().Select(x => new ImportExcel
            {
                ImportExcelId = x.ImportExcelId,
                Race = x.Race,
                ChipNumberColumn = x.ChipNumberColumn,
                TeamNumberColumn = x.TeamNumberColumn,
                TimeColumn = x.TimeColumn,
                SkipRows = x.SkipRows,
                UploadDate = x.UploadDate,
                ImportDate = x.ImportDate,
                ExcelFileName = x.ExcelFileName
            });
            int count = queryable.Count();
            List<ImportExcel> pagedList = queryable.OrderBy(sort, m => m.ImportExcelId).Page(page, pageSize).ToList();
            var viewModel = new CustomPagination<ImportExcelViewModel>(pagedList.Select(ModelToViewModel), page ?? 1, pageSize, count);
            return View(viewModel);
        }

        //
        // GET: /ImportExcel/Details/5

        public ActionResult Details(int id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id);
            if (importExcel == null)
            {
                TempData.AddStatusWarning($"Excel-import met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            return View(ModelToViewModel(importExcel));
        }

        //
        // GET: /ImportExcel/Create

        public ActionResult Create()
        {
            return View(new ImportExcelViewModel());
        }

        //
        // POST: /ImportExcel/Create

        [HttpPost]
        public ActionResult Create(ImportExcelViewModel viewModel, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var importExcel = viewModel.MapToNew<ImportExcel>();
            importExcel.ChipNumberColumn = LetterToColumn(viewModel.ChipNumberLetter);
            importExcel.TeamNumberColumn = LetterToColumn(viewModel.TeamNumberLetter);
            importExcel.TimeColumn = LetterToColumn2(viewModel.TimeLetter);
            importExcel.UploadDate = DateTime.Now;
            if (file != null && file.ContentLength > 0 && Regex.IsMatch(file.ContentType, @"excel|spreadsheet", RegexOptions.IgnoreCase))
            {
                try
                {
                    importExcel.ExcelFileName = file.FileName;
                    importExcel.ExcelData = StreamToBytes(file.InputStream);
                    _importExcelRepository.Add(importExcel);
                    _importExcelRepository.Save();
                    TempData.AddStatusInfo($"Nieuwe Excel-import {importExcel.ImportExcelId} opgeslagen.");
                }
                catch (Exception e)
                {
                    TempData.AddStatusError($"Fout bij opslaan: {e.Message}.");
                }
            }
            else
            {
                return View(viewModel);
            }

            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", ModelToViewModel(importExcel));
            }

            return RedirectToAction("Index");
        }

        //
        // GET: /ImportExcel/Edit/5

        public ActionResult Edit(int id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id);
            if (importExcel == null)
            {
                TempData.AddStatusWarning($"Excel-import met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            ImportExcelViewModel viewModel = ModelToViewModel(importExcel);
            return View(viewModel);
        }

        //
        // POST or PUT: /ImportExcel/Edit/5

        [AcceptVerbs("POST", "PUT")]
        public ActionResult Edit(ImportExcelViewModel viewModel, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            ImportExcel importExcel = _importExcelRepository.FindBy(viewModel.ImportExcelId);
            importExcel.InjectFrom(viewModel);
            importExcel.ChipNumberColumn = LetterToColumn(viewModel.ChipNumberLetter);
            importExcel.TeamNumberColumn = LetterToColumn(viewModel.TeamNumberLetter);
            importExcel.TimeColumn = LetterToColumn2(viewModel.TimeLetter);
            if (file != null && file.ContentLength > 0 && Regex.IsMatch(file.ContentType, @"excel|spreadsheet", RegexOptions.IgnoreCase))
            {
                try
                {
                    importExcel.ExcelFileName = file.FileName;
                    importExcel.UploadDate = DateTime.Now;
                    importExcel.ImportDate = null;
                    importExcel.ExcelData = StreamToBytes(file.InputStream);
                    TempData.AddStatusInfo("File-data vervangen.");
                }
                catch (Exception e)
                {
                    TempData.AddStatusError($"Fout bij opslaan: {e.Message}.");
                }
            }

            _importExcelRepository.Update(importExcel);
            _importExcelRepository.Save();
            TempData.AddStatusInfo($"Excel-import {importExcel.ImportExcelId} gewijzigd.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", ModelToViewModel(importExcel));
            }

            return RedirectToAction("Index");
        }

        //
        // GET: /ImportExcel/Delete/5

        public ActionResult Delete(int id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id);
            if (importExcel == null)
            {
                TempData.AddStatusWarning($"Excel-import met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            return View(ModelToViewModel(importExcel));
        }

        //
        // POST or DELETE: /ImportExcel/Delete/5

        [AcceptVerbs("POST", "DELETE"), ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                ImportExcel importExcel = _importExcelRepository.FindBy(id);
                if (importExcel == null)
                {
                    return RedirectToAction("Index");
                }

                _importExcelRepository.Delete(importExcel);
                _importExcelRepository.Save();
                TempData.AddStatusInfo($"Excel-import {importExcel.ImportExcelId} verwijderd.");
            }
            catch (Exception e)
            {
                TempData.AddStatusError($"Kan Excel-import {id} niet verwijderen. {(e.InnerException != null ? e.InnerException.Message : e.Message)}");
            }

            return HttpContext.Request.IsAjaxRequest() ? TempData.MessageSummary() : RedirectToAction("Index");
        }

        public ActionResult Import(int id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id);
            try
            {
                int sheetsCount = importExcel.ExcelData.GetSheetsCount(GemBoxOptionsRetriever.GetLoadOptions(importExcel.ExcelFileName));
                if (sheetsCount > 1)
                {
                    TempData.AddStatusWarning($"Dit Excel-werkboek bevat {sheetsCount} werkbladen met gegevens. Alleen het eerste werkblad wordt verwerkt.");
                }

                importExcel.Import(_participantRepository);
                importExcel.ImportDate = DateTime.Now;
                _importExcelRepository.Update(importExcel);
                _importExcelRepository.Save();
                TempData.AddStatusInfo("Importactie voltooid.");
            }
            catch (Exception exception)
            {
                TempData.AddStatusError(string.Join(" *** ", Traverse(exception).Select(x => x.Message)));
            }

            return RedirectToAction("Index");
        }

        private static IEnumerable<Exception> Traverse(Exception root)
        {
            Exception temp = root;
            while (temp != null)
            {
                yield return temp;
                temp = temp.InnerException;
            }
        }

        public FileResult Download(int id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id);
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{importExcel.ExcelFileName}\"");
            return new FileContentResult(importExcel.ExcelData, "application/vnd.ms-excel");
        }

        private static ImportExcelViewModel ModelToViewModel(ImportExcel m)
        {
            var viewModel = m.MapToNew<ImportExcelViewModel>();
            viewModel.ChipNumberLetter = ColumnToLetter(m.ChipNumberColumn);
            viewModel.TeamNumberLetter = ColumnToLetter(m.TeamNumberColumn);
            viewModel.TimeLetter = ColumnToLetter(m.TimeColumn);
            return viewModel;
        }

        private static int? LetterToColumn(string letter)
        {
            return string.IsNullOrEmpty(letter) ? (int?)null : letter[0] - 'A' + 1;
        }

        private static int LetterToColumn2(string letter)
        {
            return string.IsNullOrEmpty(letter) ? 0 : letter[0] - 'A' + 1;
        }

        private static string ColumnToLetter(int? column)
        {
            return column == null ? "" : ((char)(column + 'A' - 1)).ToString(CultureInfo.InvariantCulture);
        }

        [HttpPost]
        public ActionResult ClearColumn(string id)
        {
            foreach (Participant participant in _participantRepository.All())
            {
                if (id == Parts.Skating)
                {
                    participant.Skating.Time = null;
                    participant.Skating.Score = 0;
                }
                else if (id == Parts.Running)
                {
                    participant.Running.Time = null;
                    participant.Running.Score = 0;
                }
                else if (id == Parts.Cycling)
                {
                    participant.Cycling.Time = null;
                    participant.Cycling.Score = 0;
                }
                else if (id == Parts.Rowing)
                {
                    participant.Rowing.Time = null;
                    participant.Rowing.TimeCorrected = null;
                    participant.Rowing.Score = 0;
                }

                _participantRepository.Update(participant);
            }

            _participantRepository.Save();

            foreach (CalculationReport calculationReport in _calculationReportRepository.FilterBy(x => x.Part == id))
            {
                _calculationReportRepository.Delete(calculationReport);
            }

            _calculationReportRepository.Save();

            TempData.AddStatusInfo($"Tijden en scores naar naar nul gezet voor onderdeel {id}.");
            return RedirectToAction("Index");
        }

        private static byte[] StreamToBytes(Stream stream)
        {
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return ms.GetBuffer();
            }
        }
    }
}
