﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILoginRepository _loginRepository;

        public AccountController(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        //
        // GET: /Account/Login

        public ActionResult Login()
        {
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        public ActionResult Login(UserViewModel viewModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            Login login = _loginRepository.FilterBy(u => u.UserName == viewModel.UserName).FirstOrDefault();
            if (login != null && login.Password == viewModel.Password)
            {
                if (login.RememberMe != viewModel.RememberMe)
                {
                    login.RememberMe = viewModel.RememberMe;
                    _loginRepository.Update(login);
                    _loginRepository.Save();
                }
                FormsAuthentication.SetAuthCookie(viewModel.UserName, viewModel.RememberMe);
                return Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//")
                       && !returnUrl.StartsWith("/\\")
                           ? (ActionResult)Redirect(returnUrl)
                           : RedirectToAction("Index", "Home", new {area = ""});
            }
            ModelState.AddModelError("", "The user name or password provided is incorrect.");

            // If we got this far, something failed, redisplay form
            return View(viewModel);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home", new {area = ""});
        }
    }
}