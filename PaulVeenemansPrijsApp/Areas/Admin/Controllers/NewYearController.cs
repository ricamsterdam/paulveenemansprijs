﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Mtk.Domain.NHibernate;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.StatusMessage;
using NHibernate;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize(Users = "roel")] public class NewYearController : MtkController
    {
        private readonly ILoginRepository _loginRepository;
        private readonly IRaceGroupRepository _raceGroupRepository;
        private readonly IMtkTransactionProvider _transactionProvider;
        private readonly IYearRepository _yearRepository;

        public NewYearController(ILoginRepository loginRepository, IRaceGroupRepository raceGroupRepository, IYearRepository yearRepository,
            IMtkTransactionProvider transactionProvider)
        {
            _loginRepository = loginRepository;
            _raceGroupRepository = raceGroupRepository;
            _yearRepository = yearRepository;
            _transactionProvider = transactionProvider;
        }

        [HttpGet]
        public ViewResult Index()
        {
            Year year = _yearRepository.All().FirstOrDefault();
            Login login = _loginRepository.FilterBy(x => x.UserName != "roel").FirstOrDefault();
            return View(new NewYearModel { Year = year?.Name, LoginUserName = login?.UserName, LoginPassword = login?.Password });
        }

        [HttpPost]
        public ActionResult TruncateTables()
        {
            try
            {
                _transactionProvider.BeginTransaction();
                foreach (var table in new[]
                         {
                             "calculation_report",
                             "import_excel",
                             "participant",
                             "participant_approved",
                             "participant_archived",
                             "participant_excel",
                             "remark"
                         })
                {
                    ISQLQuery query = _transactionProvider.Session.CreateSQLQuery($"TRUNCATE TABLE {table};");
                    query.ExecuteUpdate();
                }

                _transactionProvider.Commit();
                TempData.AddStatusInfo("Tabellen leeggemaakt");
            }
            catch (Exception e)
            {
                try
                {
                    _transactionProvider.Rollback();
                }
                catch
                {
                    // pass
                }

                TempData.AddStatusError(e.Message);
                if (e.InnerException != null)
                {
                    TempData.AddStatusError(e.InnerException.Message);
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Update(NewYearModel yearModel)
        {
            var years = _yearRepository.All().ToArray();
            if (years.Any())
            {
                foreach (Year year in years)
                {
                    _yearRepository.Delete(year);
                }

                _yearRepository.Save();
            }

            _yearRepository.Add(new Year { Name = yearModel.Year });
            _yearRepository.Save();
            TempData.AddStatusInfo("Jaar bijgewerkt");

            var raceGroups = _raceGroupRepository.All().ToArray();
            if (raceGroups.Any())
            {
                foreach (RaceGroup raceGroup in raceGroups)
                {
                    raceGroup.RaceGroupDescription = Regex.Replace(raceGroup.RaceGroupDescription, @"\d{4}", yearModel.Year);
                    _raceGroupRepository.Update(raceGroup);
                }

                _raceGroupRepository.Save();
            }

            TempData.AddStatusInfo("Racegroepen bijgewerkt");

            var logins = _loginRepository.FilterBy(x => x.UserName != "roel").ToArray();
            if (logins.Any())
            {
                foreach (Login login in logins)
                {
                    _loginRepository.Delete(login);
                }
            }

            _loginRepository.Add(new Login { UserName = yearModel.LoginUserName, Password = yearModel.LoginPassword });
            _loginRepository.Save();
            TempData.AddStatusInfo("Gebruikersnaam/wachtwoord bijgewerkt");

            return RedirectToAction("Index");
        }
    }
}