﻿using System.Web.Mvc;
using Mtk.Site.Mvc;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    public class HelpController : MtkController
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
    }
}