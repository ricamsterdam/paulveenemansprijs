using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.Pagination;
using Mtk.Site.Mvc.StatusMessage;
using Mtk.Site.Mvc.UI.Grid;
using Mtk.Site.Utility;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize]
    public class RemarkController : MtkController
    {
        private readonly IRaceGroupRepository _raceGroupRepository;
        private readonly IRemarkRepository _remarkRepository;

        public RemarkController(IRemarkRepository repository, IRaceGroupRepository raceGroupRepository)
        {
            _remarkRepository = repository;
            _raceGroupRepository = raceGroupRepository;
        }

        //
        // GET: /Remark/

        [HttpGet]
        public ViewResult Index(GridSortOptions sort, int? page)
        {
            const int pageSize = 20;
            ViewBag.sort = sort;
            IQueryable<Remark> queryable = _remarkRepository.All();
            int count = queryable.Count();
            List<Remark> pagedList = queryable.OrderBy(sort, m => m.RemarkId).Page(page, pageSize).ToList();
            var viewModel = new CustomPagination<RemarkViewModel>(pagedList.Select(m => m.MapToNew<RemarkViewModel, FlatLoopValueInjection>()),
                page ?? 1,
                pageSize,
                count);
            return View(viewModel);
        }

        //
        // GET: /Remark/Details/5

        public ActionResult Details(int id)
        {
            Remark remark = _remarkRepository.FindBy(id);
            if (remark == null)
            {
                TempData.AddStatusWarning($"Opmerking met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            return View(remark.MapToNew<RemarkViewModel, FlatLoopValueInjection>());
        }

        //
        // GET: /Remark/Create

        public ActionResult Create()
        {
            var viewModel = new RemarkViewModel {RaceGroups = RaceGroupSelectList(0)};
            return View(viewModel);
        }

        //
        // POST or PUT: /Remark/Create

        [HttpPost]
        public ActionResult Create(RemarkViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
                return View(viewModel);
            }
            var remark = new Remark {RaceGroup = _raceGroupRepository.Proxy(viewModel.RaceGroupRaceGroupId), RemarkText = viewModel.RemarkText};
            _remarkRepository.Add(remark);
            _remarkRepository.Save();
            TempData.AddStatusInfo($"Nieuwe opmerking {remark.RemarkId} opgeslagen.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", remark.MapToNew<RemarkViewModel, FlatLoopValueInjection>());
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Remark/Edit/5

        public ActionResult Edit(int id)
        {
            Remark remark = _remarkRepository.FindBy(id);
            if (remark == null)
            {
                TempData.AddStatusWarning($"Opmerking met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            RemarkViewModel viewModel = remark.MapToNew<RemarkViewModel, FlatLoopValueInjection>();
            viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
            return View(viewModel);
        }

        //
        // POST or PUT: /Remark/Edit/5

        [AcceptVerbs("POST", "PUT")]
        public ActionResult Edit(RemarkViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
                return View(viewModel);
            }
            Remark remark = _remarkRepository.FindBy(viewModel.RemarkId);
            remark.RaceGroup = _raceGroupRepository.Proxy(viewModel.RaceGroupRaceGroupId);
            remark.RemarkText = viewModel.RemarkText;
            _remarkRepository.Update(remark);
            _remarkRepository.Save();
            TempData.AddStatusInfo($"Opmerking {remark.RemarkId} gewijzigd.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", remark.MapToNew<RemarkViewModel, FlatLoopValueInjection>());
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Remark/Delete/5

        public ActionResult Delete(int id)
        {
            Remark remark = _remarkRepository.FindBy(id);
            if (remark == null)
            {
                TempData.AddStatusWarning($"Opmerking met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            return View(remark.MapToNew<RemarkViewModel, FlatLoopValueInjection>());
        }

        //
        // POST or DELETE: /Remark/Delete/5

        [AcceptVerbs("POST", "DELETE"), ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                Remark remark = _remarkRepository.FindBy(id);
                if (remark == null)
                {
                    return RedirectToAction("Index");
                }
                _remarkRepository.Delete(remark);
                _remarkRepository.Save();
                TempData.AddStatusInfo($"Opmerking {remark.RemarkId} verwijderd.");
            }
            catch (Exception e)
            {
                TempData.AddStatusError($"Kan opmerking {id} niet verwijderen. {(e.InnerException != null ? e.InnerException.Message : e.Message)}");
            }
            return HttpContext.Request.IsAjaxRequest() ? TempData.MessageSummary() : RedirectToAction("Index");
        }

        private IEnumerable<SelectListItem> RaceGroupSelectList(int selected)
        {
            List<RaceGroup> raceGroups = _raceGroupRepository.All().ToList();
            return
                raceGroups.Select(
                    r =>
                        new SelectListItem
                        {
                            Value = r.RaceGroupId.ToString(CultureInfo.InvariantCulture),
                            Text = r.RaceGroupDescription,
                            Selected = r.RaceGroupId == selected
                        });
        }
    }
}