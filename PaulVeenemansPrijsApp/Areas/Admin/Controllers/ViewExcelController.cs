using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using GemBox.Spreadsheet;
using HtmlAgilityPack;
using Mtk.Site.Mvc;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    public class ViewExcelController : MtkController
    {
        private readonly ImportExcelRepository _importExcelRepository;

        public ViewExcelController(ImportExcelRepository importExcelRepository)
        {
            _importExcelRepository = importExcelRepository;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "ImportExcel");
        }

        public ActionResult View(int? id)
        {
            RemoveOldWorkDirectory();

            ImportExcel importExcel = _importExcelRepository.FindBy(id.GetValueOrDefault(0));
            if (importExcel == null)
            {
                return RedirectToAction("Index", "ImportExcel");
            }

            ViewBag.Id = id;
            ViewBag.Title = importExcel.ExcelFileName;
            ViewBag.LegendChipTeamText = importExcel.ChipNumberColumn != null ? "Chipnummer" : importExcel.TeamNumberColumn != null ? "Teamnummer" : "Onbekend";
            return View();
        }

        public ActionResult SheetContents(int? id)
        {
            ImportExcel importExcel = _importExcelRepository.FindBy(id.GetValueOrDefault(0));

            using (var memoryStream = new MemoryStream(importExcel.ExcelData))
            {
                string html = GetFirstSheetFromStreamAndReturnHtml(memoryStream,
                    GemBoxOptionsRetriever.GetLoadOptions(importExcel.ExcelFileName),
                    (importExcel.ChipNumberColumn ?? importExcel.TeamNumberColumn.GetValueOrDefault(1)) - 1,
                    importExcel.TimeColumn - 1,
                    importExcel.SkipRows);
                return Content(PostProcess(html), "text/html", Encoding.UTF8);
            }
        }

        private static string GetFirstSheetFromStreamAndReturnHtml(Stream stream, LoadOptions loadOptions, int markedColumn1, int markedColumn2, int skipRows)
        {
            ExcelFile book = ExcelFile.Load(stream, loadOptions);
            for (int i = book.Worksheets.Count - 1; i > 0; i--)
            {
                book.Worksheets.Remove(i);
            }

            try
            {
                ExcelWorksheet sheet = book.Worksheets[0];
                sheet.Columns[markedColumn1].Style.FillPattern.PatternStyle = FillPatternStyle.Solid;
                sheet.Columns[markedColumn1].Style.FillPattern.PatternForegroundColor = Color.FromArgb(255, 255, 0);
                sheet.Columns[markedColumn2].Style.FillPattern.PatternStyle = FillPatternStyle.Solid;
                sheet.Columns[markedColumn2].Style.FillPattern.PatternForegroundColor = Color.FromArgb(0, 205, 255);
                for (var r = 0; r < skipRows; r++)
                {
                    sheet.Rows[r].Style.FillPattern.PatternStyle = FillPatternStyle.Solid;
                    sheet.Rows[r].Style.FillPattern.PatternForegroundColor = Color.FromArgb(205, 102, 205);
                }

                book.Worksheets[0].SelectedRanges.Add(book.Worksheets[0], "A1");
                book.Worksheets[0].ViewOptions.FirstVisibleColumn = 0;
                book.Worksheets[0].ViewOptions.FirstVisibleRow = 0;
                book.Worksheets[0].SelectedRanges.Clear();
            }
            catch
            {
                // Skip.
            }

            using (var stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter,
                    new XmlWriterSettings {Encoding = Encoding.UTF8, Indent = true, OmitXmlDeclaration = true}))
                {
                    HtmlSaveOptions saveOptions = SaveOptions.HtmlDefault;
                    saveOptions.Encoding = Encoding.UTF8;
                    saveOptions.HtmlType = HtmlType.Html;
                    saveOptions.WriteCellAddress = true;
                    book.Save(xmlWriter, saveOptions);
                }

                return stringWriter.ToString();
            }
        }

        private static string PostProcess(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            AddStyle();

            HtmlNode table = doc.DocumentNode.SelectSingleNode("//table");
            if (table == null)
            {
                return html;
            }

            AddRowNumbers(table);
            AddColumnLetters(table);

            using (var stringWriter = new StringWriter())
            {
                doc.Save(stringWriter);
                return stringWriter.ToString();
            }

            void AddStyle()
            {
                HtmlNode head = doc.DocumentNode.SelectSingleNode("//head");
                head.AppendChild(HtmlNode.CreateNode(string.Join(" ",
                    "<style>",
                    "td, th { border: 1px solid #d0d0d0; }",
                    ".excel-head { background-color: #e8e8e8; font-weight: 400; text-align: center; }",
                    "</style>")));
            }

            void AddRowNumbers(HtmlNode t)
            {
                t.PrependChild(HtmlNode.CreateNode("<col style=\"width: 36px;\">"));

                var r = 0;
                foreach (HtmlNode row in t.SelectNodes("//tr"))
                {
                    row.PrependChild(HtmlNode.CreateNode($"<td class=\"excel-head\">{++r}</td>"));
                }
            }

            void AddColumnLetters(HtmlNode t)
            {
                HtmlNode headerRow = HtmlNode.CreateNode("<tr><th class=\"excel-head\"></th></tr>");

                for (var c = 0; c < t.SelectNodes("//col").Sum(x => x.GetAttributeValue("span", 1)) - 1; c++)
                {
                    var columnLetter = new string(IntToLetters(c).Reverse().ToArray());
                    headerRow.AppendChild(HtmlNode.CreateNode($"<th class=\"excel-head\">{columnLetter}</th>"));
                }

                t.InsertBefore(headerRow, t.SelectSingleNode("//tr"));
            }
        }

        private void RemoveOldWorkDirectory()
        {
            string directory = Server.MapPath("~/App_Data/WorkDirectory");
            try
            {
                if (Directory.Exists(directory))
                {
                    Directory.Delete(directory, true);
                }
            }
            catch
            {
                // Skip.
            }
        }

        private static IEnumerable<char> IntToLetters(int number)
        {
            while (number >= 0)
            {
                yield return (char)(65 + number % 26);
                number = number / 26 - 1;
            }
        }
    }
}
