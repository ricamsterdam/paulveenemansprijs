﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.Pagination;
using Mtk.Site.Mvc.StatusMessage;
using Mtk.Site.Mvc.UI.Grid;
using Mtk.Site.Utility;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize]
    public class ParticipantController : MtkController
    {
        private readonly IParticipantExcelRepository _participantExcelRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly IRaceGroupRepository _raceGroupRepository;
        private readonly IStatusRepository _statusRepository;

        public ParticipantController(IParticipantRepository repository, IRaceGroupRepository raceGroupRepository, IStatusRepository statusRepository,
            IParticipantExcelRepository participantExcelRepository)
        {
            _participantRepository = repository;
            _raceGroupRepository = raceGroupRepository;
            _statusRepository = statusRepository;
            _participantExcelRepository = participantExcelRepository;
        }

        //
        // GET: /Participant/

        [HttpGet]
        public ViewResult Index(GridSortOptions sort, int? page, string filter)
        {
            const int pageSize = 20;
            ViewBag.sort = sort;
            ViewBag.filter = filter;
            int numericFilter = !string.IsNullOrEmpty(filter) && Regex.IsMatch(filter, @"^\d+$") ? int.Parse(filter) : 0;
            IQueryable<Participant> queryable =
                _participantRepository.FilterBy(
                    x =>
                    string.IsNullOrEmpty(filter) || x.ChipNumber.Contains(filter) || x.ParticipantName.Contains(filter) || x.TeamSeqNumber == numericFilter
                    || x.TeamCode.Contains(filter));
            int count = queryable.Count();
            List<Participant> pagedList = queryable.OrderBy(sort, m => m.ParticipantId).Page(page, pageSize).ToList();
            var viewModel = new CustomPagination<ParticipantViewModel>(pagedList.Select(m => m.MapToNewParticipantViewModel()), page ?? 1, pageSize, count);
            return View(viewModel);
        }

        //
        // GET: /Participant/Details/5

        public ActionResult Details(int id)
        {
            Participant participant = _participantRepository.FindBy(id);
            if (participant == null)
            {
                TempData.AddStatusWarning($"Deelnemer met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            return View(participant.MapToNewParticipantViewModel());
        }

        //
        // GET: /Participant/Create

        public ActionResult Create()
        {
            return View(new ParticipantViewModel {RaceGroups = RaceGroupSelectList(0), Statuses = StatusSelectList()});
        }

        //
        // POST or PUT: /Participant/Create

        [HttpPost]
        public ActionResult Create(ParticipantViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
                viewModel.Statuses = StatusSelectList();
                return View(viewModel);
            }

            var participant = new Participant {Skating = new Part(), Running = new Part(), Cycling = new Part(), Rowing = new RowingPart()};
            MapParticipantViewModelToParticipant(viewModel, participant);
            _participantRepository.Add(participant);
            _participantRepository.Save();
            TempData.AddStatusInfo($"Nieuwe deelnemer {participant.ParticipantId} opgeslagen.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", participant.MapToNewParticipantViewModel());
            }

            return RedirectToAction("Index");
        }

        //
        // GET: /Participant/Edit/5

        public ActionResult Edit(int id)
        {
            Participant participant = _participantRepository.FindBy(id);
            if (participant == null)
            {
                TempData.AddStatusWarning($"Deelnemer met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            ParticipantViewModel viewModel = participant.MapToNewParticipantViewModel();
            viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
            viewModel.Statuses = StatusSelectList();
            return View(viewModel);
        }

        //
        // POST or PUT: /Participant/Edit/5

        [AcceptVerbs("POST", "PUT")]
        public ActionResult Edit(ParticipantViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.RaceGroups = RaceGroupSelectList(viewModel.RaceGroupRaceGroupId);
                viewModel.Statuses = StatusSelectList();
                return View(viewModel);
            }

            Participant participant = _participantRepository.FindBy(viewModel.ParticipantId);
            MapParticipantViewModelToParticipant(viewModel, participant);
            _participantRepository.Update(participant);
            _participantRepository.Save();
            TempData.AddStatusInfo($"Deelnemer {participant.ParticipantId} gewijzigd.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", participant.MapToNewParticipantViewModel());
            }

            return RedirectToAction("Index");
        }

        //
        // GET: /Participant/Delete/5

        public ActionResult Delete(int id)
        {
            Participant participant = _participantRepository.FindBy(id);
            if (participant == null)
            {
                TempData.AddStatusWarning($"Deelnemer met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }

            return View(participant.MapToNewParticipantViewModel());
        }

        //
        // POST or DELETE: /Participant/Delete/5

        [AcceptVerbs("POST", "DELETE"), ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                Participant participant = _participantRepository.FindBy(id);
                if (participant == null)
                {
                    return RedirectToAction("Index");
                }

                _participantRepository.Delete(participant);
                _participantRepository.Save();
                TempData.AddStatusInfo($"Deelnemer {participant.ParticipantId} verwijderd.");
            }
            catch (Exception e)
            {
                TempData.AddStatusError($"Kan deelnemer {id} niet verwijderen. {(e.InnerException != null ? e.InnerException.Message : e.Message)}");
            }

            return HttpContext.Request.IsAjaxRequest() ? TempData.MessageSummary() : RedirectToAction("Index");
        }

        public ActionResult Import()
        {
            return View();
        }

        [HttpPost, ActionName("Import")]
        public ActionResult ImportConfirmed(HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength <= 0 || !Regex.IsMatch(file.ContentType, @"excel|spreadsheet", RegexOptions.IgnoreCase))
            {
                ModelState.AddModelError("", "File must be an Excel document.");
                return View();
            }

            IQueryable<Participant> participants = _participantRepository.All();
            if (participants.Any())
            {
                foreach (Participant participant in participants)
                {
                    _participantRepository.Delete(participant);
                }

                TempData.AddStatusInfo("Oude deelnemers verwijderd.");
            }

            byte[] data;
            using (var ms = new MemoryStream())
            {
                file.InputStream.CopyTo(ms);
                data = ms.GetBuffer();
            }

            try
            {
                _participantExcelRepository.Add(new ParticipantExcel {ExcelFileName = file.FileName, ExcelData = data});
                _participantExcelRepository.Save();
            }
            catch (Exception e)
            {
                TempData.AddStatusWarning($"Excel document could not be saved in the database. {e.Message}");
            }

            var loadOptions = GemBoxOptionsRetriever.GetLoadOptions(file.FileName);
            try
            {
                int sheetsCount = data.GetSheetsCount(loadOptions);
                if (sheetsCount > 1)
                {
                    TempData.AddStatusWarning($"Dit Excel-werkboek bevat {sheetsCount} werkbladen met gegevens. Alleen het eerste werkblad wordt verwerkt.");
                }

                data.Import(loadOptions, _participantRepository, _raceGroupRepository, _statusRepository);
            }
            catch (Exception e)
            {
                TempData.AddStatusError($"Importfout. {e.Message}");
                return RedirectToAction("Index");
            }

            TempData.AddStatusInfo("Nieuwe deelnemers geïmporteerd.");
            return RedirectToAction("Index");
        }

        public ActionResult Download()
        {
            ParticipantExcel participantExcel = _participantExcelRepository.All().OrderByDescending(x => x.ParticipantExcelId).FirstOrDefault();
            if (participantExcel != null)
            {
                return File(participantExcel.ExcelData, "application/vnd.excel", participantExcel.ExcelFileName);
            }

            return new HttpNotFoundResult();
        }

        private void MapParticipantViewModelToParticipant(ParticipantViewModel viewModel, Participant participant)
        {
            participant.InjectFrom<UnflatLoopValueInjectionWithTime>(viewModel);
            participant.Skating.Status = _statusRepository.Proxy(viewModel.SkatingStatusStatusId);
            participant.Running.Status = _statusRepository.Proxy(viewModel.RunningStatusStatusId);
            participant.Cycling.Status = _statusRepository.Proxy(viewModel.CyclingStatusStatusId);
            participant.Rowing.Status = _statusRepository.Proxy(viewModel.RowingStatusStatusId);
            participant.RaceGroup = _raceGroupRepository.Proxy(viewModel.RaceGroupRaceGroupId);
        }

        private IEnumerable<SelectListItem> RaceGroupSelectList(int selected)
        {
            List<RaceGroup> raceGroups = _raceGroupRepository.All().ToList();
            return
                raceGroups.Select(
                    r =>
                    new SelectListItem
                        {
                            Value = r.RaceGroupId.ToString(CultureInfo.InvariantCulture),
                            Text = r.RaceGroupDescription,
                            Selected = r.RaceGroupId == selected
                        });
        }

        private IEnumerable<SelectListItem> StatusSelectList()
        {
            List<Status> statuses = _statusRepository.All().ToList();
            return statuses.Select(r => new SelectListItem {Value = r.StatusId.ToString(CultureInfo.InvariantCulture), Text = r.Description});
        }
    }
}
