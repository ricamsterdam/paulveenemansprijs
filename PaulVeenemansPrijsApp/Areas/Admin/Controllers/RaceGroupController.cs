using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.Pagination;
using Mtk.Site.Mvc.StatusMessage;
using Mtk.Site.Mvc.UI.Grid;
using Mtk.Site.Utility;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize]
    public class RaceGroupController : MtkController
    {
        private readonly IRaceGroupRepository _repository;

        public RaceGroupController(IRaceGroupRepository repository)
        {
            _repository = repository;
        }

        //
        // GET: /RaceGroup/

        [HttpGet]
        public ViewResult Index(GridSortOptions sort, int? page)
        {
            const int pageSize = 20;
            ViewBag.sort = sort;
            IQueryable<RaceGroup> queryable = _repository.All();
            int count = queryable.Count();
            List<RaceGroup> pagedList = queryable.OrderBy(sort, m => m.RaceGroupId).Page(page, pageSize).ToList();
            var viewModel = new CustomPagination<RaceGroupViewModel>(pagedList.Select(m => m.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>()),
                page ?? 1,
                pageSize,
                count);
            return View(viewModel);
        }

        //
        // GET: /RaceGroup/Details/5

        public ActionResult Details(int id)
        {
            RaceGroup raceGroup = _repository.FindBy(id);
            if (raceGroup == null)
            {
                TempData.AddStatusWarning($"Racegroep met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            return View(raceGroup.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>());
        }

        //
        // GET: /RaceGroup/Create

        public ActionResult Create()
        {
            return View(new RaceGroupViewModel());
        }

        //
        // POST or PUT: /RaceGroup/Create

        [HttpPost]
        public ActionResult Create(RaceGroupViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            var raceGroup = viewModel.MapToNew<RaceGroup>();
            _repository.Add(raceGroup);
            _repository.Save();
            TempData.AddStatusInfo($"Nieuwe racegroep {raceGroup.RaceGroupId} opgeslagen.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", raceGroup.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>());
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /RaceGroup/Edit/5

        public ActionResult Edit(int id)
        {
            RaceGroup raceGroup = _repository.FindBy(id);
            if (raceGroup == null)
            {
                TempData.AddStatusWarning($"Racegroep met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            RaceGroupViewModel viewModel = raceGroup.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>();
            return View(viewModel);
        }

        //
        // POST or PUT: /RaceGroup/Edit/5

        [AcceptVerbs("POST", "PUT")]
        public ActionResult Edit(RaceGroupViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            RaceGroup raceGroup = _repository.FindBy(viewModel.RaceGroupId);
            raceGroup.InjectFrom(viewModel);
            _repository.Update(raceGroup);
            _repository.Save();
            TempData.AddStatusInfo($"Racegroep {raceGroup.RaceGroupId} gewijzigd.");
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("GridRow", raceGroup.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>());
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /RaceGroup/Delete/5

        public ActionResult Delete(int id)
        {
            RaceGroup raceGroup = _repository.FindBy(id);
            if (raceGroup == null)
            {
                TempData.AddStatusWarning($"Racegroep met id {id} niet gevonden.");
                return RedirectToAction("Index");
            }
            return View(raceGroup.MapToNew<RaceGroupViewModel, FlatLoopValueInjection>());
        }

        //
        // POST or DELETE: /RaceGroup/Delete/5

        [AcceptVerbs("POST", "DELETE"), ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                RaceGroup raceGroup = _repository.FindBy(id);
                if (raceGroup == null)
                {
                    return RedirectToAction("Index");
                }
                _repository.Delete(raceGroup);
                _repository.Save();
                TempData.AddStatusInfo($"Racegroep {raceGroup.RaceGroupId} verwijderd.");
            }
            catch (Exception e)
            {
                TempData.AddStatusError($"Kan racegroep {id} niet verwijderen. {(e.InnerException != null ? e.InnerException.Message : e.Message)}");
            }
            return HttpContext.Request.IsAjaxRequest() ? TempData.MessageSummary() : RedirectToAction("Index");
        }
    }
}