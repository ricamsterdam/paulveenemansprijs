using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mtk.Site.Mvc;
using Mtk.Site.Mvc.StatusMessage;
using Mtk.Site.Mvc.UI.Grid;
using PaulVeenemansPrijsDomain.BusinessLogic;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.Models;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Controllers
{
    [Authorize]
    public class ApprovalController : MtkController
    {
        private readonly IParticipantApprovedRepository _participantApprovedRepository;
        private readonly IParticipantArchivedRepository _participantArchivedRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly IRaceGroupRepository _raceGroupRepository;
        private readonly ICalculationReportRepository _calculationReportRepository;

        public ApprovalController(IParticipantRepository participantRepository, IParticipantApprovedRepository participantApprovedRepository,
            IParticipantArchivedRepository participantArchivedRepository, IRaceGroupRepository raceGroupRepository,
            ICalculationReportRepository calculationReportRepository)
        {
            _participantRepository = participantRepository;
            _participantApprovedRepository = participantApprovedRepository;
            _participantArchivedRepository = participantArchivedRepository;
            _raceGroupRepository = raceGroupRepository;
            _calculationReportRepository = calculationReportRepository;
        }

        //
        // GET: /Approval/

        [HttpGet]
        public ViewResult Index(GridSortOptions sort, int? page)
        {
            var raceGroupViewModels = new List<RaceGroupViewModel>();
            foreach (RaceGroup raceGroup in _raceGroupRepository.All().OrderBy(r => r.RaceGroupId))
            {
                var raceGroupViewModel = raceGroup.MapToNew<RaceGroupViewModel>();
                var i = 1;
                raceGroupViewModel.Participants = raceGroup.Participants.OrderByDescending(p => p.OverallScore)
                    .Select(x => x.MapToNewParticipantViewModel(ref i)).ToList();
                raceGroupViewModel.Remarks = raceGroup.Remarks.Select(x => x.MapToNew<RemarkViewModel>()).ToList();
                raceGroupViewModel.CalculationReports = raceGroup.CalculationReports.Select(x => x.MapToNew<CalculationReportViewModel>()).ToList();
                raceGroupViewModels.Add(raceGroupViewModel);
            }
            return View(new ResultViewModel {RaceGroups = raceGroupViewModels});
        }

        [HttpPost]
        public ActionResult Calculate(string id)
        {
            foreach (RaceGroup raceGroup in _raceGroupRepository.All())
            {
                string reportDescription = ScoreCalculation.Calculate(id, raceGroup.RaceGroupId, _participantRepository);
                _participantRepository.Save();

                SaveCalculationReport(raceGroup, id, reportDescription);

                ScoreCalculation.CalculateRankings(raceGroup.RaceGroupId, _participantRepository);
                _participantRepository.Save();
                TempData.AddStatusInfo($"Score {id} berekend voor racegroep {raceGroup.RaceGroupDescription}.");
            }
            TempData.AddStatusInfo("Rankings berekend.");
            return RedirectToAction("Index");
        }

        private void SaveCalculationReport(RaceGroup raceGroup, string part, string description)
        {
            CalculationReport calculationReport = _calculationReportRepository.FilterBy(x => x.RaceGroup == raceGroup && x.Part == part).FirstOrDefault();
            if (calculationReport != null)
            {
                calculationReport.Description = description;
                _calculationReportRepository.Update(calculationReport);
            }
            else
            {
                _calculationReportRepository.Add(new CalculationReport {RaceGroup = raceGroup, Part = part, Description = description});
            }
            _calculationReportRepository.Save();
        }

        [HttpPost]
        public ActionResult PublishOverallScore()
        {
            ScoreCalculation.ArchiveOverallScores(_participantApprovedRepository, _participantArchivedRepository);
            TempData.AddStatusInfo("Vorige gepubliceerde scores gearchiveerd.");
            ScoreCalculation.PublishOverallScores(_participantRepository, _participantApprovedRepository);
            _participantRepository.Save();
            TempData.AddStatusInfo("Score gepubliceerd.");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UnPublishOverallScore()
        {
            ScoreCalculation.UnArchiveOverallScores(_participantArchivedRepository, _participantApprovedRepository);
            _participantApprovedRepository.Save();
            TempData.AddStatusInfo("Vorige gepubliceerde score teruggezet vanuit archief.");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteOverallScore()
        {
            foreach (Participant participant in _participantRepository.All())
            {
                participant.OverallScore = 0;
                _participantRepository.Update(participant);
            }
            _participantRepository.Save();
            TempData.AddStatusInfo("Overall scores verwijderd.");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteRankings()
        {
            foreach (Participant participant in _participantRepository.All())
            {
                participant.RankMajor = null;
                participant.RankMinor = null;
                _participantRepository.Update(participant);
            }
            _participantRepository.Save();
            TempData.AddStatusInfo("Rankings verwijderd.");
            return RedirectToAction("Index");
        }

        public ActionResult Excel()
        {
            return File(new DownloadExcel(_raceGroupRepository).GetBytes(rg => rg.Participants), "application/vnd.excel",
                $"voorlopige-scores-{DateTime.Now:yyyyMMdd-HHmm}.xls");
        }
    }
}