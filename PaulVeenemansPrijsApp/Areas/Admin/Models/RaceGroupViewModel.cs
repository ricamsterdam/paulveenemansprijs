﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class RaceGroupViewModel
    {
        [Display(Name = "Id"), Key]
        public int RaceGroupId { get; set; }

        [Display(Name = "Omschrijving"), MaxLength(100, ErrorMessage = "Maximaal 100 tekens.")]
        public string RaceGroupDescription { get; set; }

        public IList<ParticipantViewModel> Participants { get; set; }
        public IList<RemarkViewModel> Remarks { get; set; }
        public IList<CalculationReportViewModel> CalculationReports { get; set; }
    }
}