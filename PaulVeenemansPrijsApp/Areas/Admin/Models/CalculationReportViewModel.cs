﻿namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class CalculationReportViewModel
    {
        public string Description { get; set; }
        public string Part { get; set; }
        public string RaceGroupRaceGroupDescription { get; set; }
    }
}
