﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class NewYearModel
    {
        [Display(Name = "Wachtwoord")] public string LoginPassword { get; set; }
        [Display(Name = "Gebruikersnaam")] public string LoginUserName { get; set; }
        [Display(Name = "Jaar")] public string Year { get; set; }
    }
}