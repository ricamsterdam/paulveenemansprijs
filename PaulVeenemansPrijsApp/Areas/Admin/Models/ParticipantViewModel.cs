﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class ParticipantViewModel
    {
        public int Index { get; set; }

        [Display(Name = "Id"), Key]
        public int ParticipantId { get; set; }

        [Display(Name = "Teamvolgnummer"), Required]
        public int TeamSeqNumber { get; set; }

        [Display(Name = "Chipnummer"), StringLength(50), Required]
        public string ChipNumber { get; set; }

        [Display(Name = "Racecode groot"), StringLength(50), Required]
        public string RaceCodeMajor { get; set; }

        [Display(Name = "Racecode klein"), StringLength(10)]
        public string RaceCodeMinor { get; set; }

        [Display(Name = "Teamcode"), StringLength(50), Required]
        public string TeamCode { get; set; }

        [Display(Name = "Naam deelnemer"), StringLength(100), Required]
        public string ParticipantName { get; set; }

        [Display(Name = "Code roeien"), RegularExpression(@"^(?:[12]x|2-)$", ErrorMessage = "Kies 1x, 2x of 2-.")]
        public string CodeRowing { get; set; }

        [Display(Name = "Schaatsen tijd"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string SkatingTime { get; set; }

        [Display(Name = "Schaatsen straf"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string SkatingPenalty { get; set; }

        [Display(Name = "Schaatsen status")]
        public int SkatingStatusStatusId { get; set; }

        [Display(Name = "Schaatsen status")]
        public string SkatingStatusCode { get; set; }

        [Display(Name = "Schaatsen status")]
        public string SkatingStatusDescription { get; set; }

        [Display(Name = "Schaatsen score"), DisplayFormat(DataFormatString = "{0:n1}")]
        public double SkatingScore { get; set; }

        [Display(Name = "Hardlopen tijd"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string RunningTime { get; set; }

        [Display(Name = "Hardlopen straf"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string RunningPenalty { get; set; }

        [Display(Name = "Hardlopen status")]
        public int RunningStatusStatusId { get; set; }

        [Display(Name = "Hardlopen status")]
        public string RunningStatusCode { get; set; }

        [Display(Name = "Hardlopen status")]
        public string RunningStatusDescription { get; set; }

        [Display(Name = "Hardlopen score"), DisplayFormat(DataFormatString = "{0:n1}")]
        public double RunningScore { get; set; }

        [Display(Name = "Wielrennen tijd"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string CyclingTime { get; set; }

        [Display(Name = "Wielrennen straf"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string CyclingPenalty { get; set; }

        [Display(Name = "Wielrennen status")]
        public int CyclingStatusStatusId { get; set; }

        [Display(Name = "Wielrennen status")]
        public string CyclingStatusCode { get; set; }

        [Display(Name = "Wielrennen status")]
        public string CyclingStatusDescription { get; set; }

        [Display(Name = "Wielrennen score"), DisplayFormat(DataFormatString = "{0:n1}")]
        public double CyclingScore { get; set; }

        [Display(Name = "Roeien tijd"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string RowingTime { get; set; }

        [Display(Name = "Roeien straf"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string RowingPenalty { get; set; }

        [Display(Name = "Roeien gecorrigeerd"), RegularExpression(@"^(?!00:00)\d\d:\d\d(?:\.\d+)?$", ErrorMessage = "Tijd als MM:SS.00 of leeg")]
        public string RowingTimeCorrected { get; set; }

        [Display(Name = "Roeien status")]
        public int RowingStatusStatusId { get; set; }

        [Display(Name = "Roeien status")]
        public string RowingStatusCode { get; set; }

        [Display(Name = "Roeien status")]
        public string RowingStatusDescription { get; set; }

        [Display(Name = "Roeien score"), DisplayFormat(DataFormatString = "{0:n1}")]
        public double RowingScore { get; set; }

        [Display(Name = "Totaalscore"), DisplayFormat(DataFormatString = "{0:n3}")]
        public double OverallScore { get; set; }

        [Display(Name = "Volgnr groot")]
        public int? RankMajor { get; set; }

        [Display(Name = "Volgnr klein")]
        public int? RankMinor { get; set; }

        [Display(Name = "Racegroep")]
        public int RaceGroupRaceGroupId { get; set; }

        [Display(Name = "Racegroep")]
        public string RaceGroupRaceGroupDescription { get; set; }

        public IEnumerable<SelectListItem> RaceGroups { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }
    }
}