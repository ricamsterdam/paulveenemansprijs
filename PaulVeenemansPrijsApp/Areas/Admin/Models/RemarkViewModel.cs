﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class RemarkViewModel
    {
        [Display(Name = "Id"), Key]
        public int RemarkId { get; set; }

        [Display(Name = "Racegroep")]
        public int RaceGroupRaceGroupId { get; set; }

        [Display(Name = "Racegroep")]
        public string RaceGroupRaceGroupDescription { get; set; }

        [Display(Name = "Opmerking"), StringLength(100, ErrorMessage = "Maximaal 100 tekens.")]
        public string RemarkText { get; set; }

        public IEnumerable<SelectListItem> RaceGroups { get; set; }
    }
}