﻿using System.ComponentModel.DataAnnotations;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    public class UserViewModel
    {
        [Required, Display(Name = "Gebruikersnaam"), Key]
        public string UserName { get; set; }

        [Required, DataType(DataType.Password), Display(Name = "Wachtwoord")]
        public string Password { get; set; }

        [Display(Name = "Gegevens onthouden")]
        public bool RememberMe { get; set; }
    }
}