﻿using System;
using System.ComponentModel.DataAnnotations;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp.Areas.Admin.Models
{
    [ExactlyOneFieldRequired("ChipNumberLetter", "TeamNumberLetter", ErrorMessage = "Chipnr/Teamnr: precies één van beide velden dient gevuld te worden.")]
    public class ImportExcelViewModel
    {
        [Display(Name = "Id"), Key]
        public int ImportExcelId { get; set; }

        [Display(Name = "Race-onderdeel"), Required, RegularExpression(@"^(?:Cycling|Rowing|Running|Skating)", ErrorMessage = "Ongeldig onderdeel.")]
        public string Race { get; set; }

        [Display(Name = "Kolom­letter ChipNr"), RegularExpression(@"^[A-Z]?$", ErrorMessage = "Letter A t/m Z toegestaan.")]
        public string ChipNumberLetter { get; set; }

        [Display(Name = "Kolom­letter TeamNr"), RegularExpression(@"^[A-Z]?$", ErrorMessage = "Letter A t/m Z toegestaan.")]
        public string TeamNumberLetter { get; set; }

        [Required, Display(Name = "Kolom­letter Tijd"), RegularExpression(@"^[A-Z]$", ErrorMessage = "Letter A t/m Z toegestaan.")]
        public string TimeLetter { get; set; }

        [Required, Display(Name = "Aantal rijen overslaan")]
        public int SkipRows { get; set; }

        [Display(Name = "Upload­datum")]
        public DateTime? UploadDate { get; set; }

        [Display(Name = "Import­datum")]
        public DateTime? ImportDate { get; set; }

        [Display(Name = "Excel-bestandsnaam")]
        public string ExcelFileName { get; set; }
    }
}
