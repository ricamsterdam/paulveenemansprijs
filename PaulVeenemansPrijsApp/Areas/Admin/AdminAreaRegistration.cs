﻿using System.Web.Mvc;
using LowercaseRoutesMVC;

namespace PaulVeenemansPrijsApp.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRouteLowercase("Admin_default", "admin/{controller}/{action}/{id}", new {action = "Index", id = UrlParameter.Optional});
        }
    }
}