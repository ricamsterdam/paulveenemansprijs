Uitslagenapplicatie Paul Veenemans Prijs
========================================

Werking systeem
---------------

Het systeem werkt als volgt:

Voorafgaand aan de wedstrijd importeer je een lijst met stamgegevens:

1.  teamnummer (startnummer)

2.  chipnummer (van MyLaps)

3.  veld (Meisjes Junioren, Jongens Junioren, Dames Senioren, Heren
    Senioren, Dames Veteranen, Heren Veteranen)

4.  subveld (leeg, of Veteranen A-J)

5.  teamcode (vereniging plus volgnummer)

6.  naam deelnemer

7.  roeien in skiff of tweezonder

8.  eventueel racegroep (=wedstrijd, mag leeggelaten worden)

Deze lijst is in Excel. De kolomvolgorde is vast, de eerste rij is een
veldnamenrij. Dit is de enige import waarbij de compleet aanwezige
inhoud eerst wordt weggegooid voordat de import begint. Bij alle overige
importacties worden bestaande gegevens aangevuld (en eventueel
overschreven).

Het Excel-document wordt bewaard in de applicatie. Je kunt het
daarvandaan weer downloaden om in Excel aanpassingen te maken.

Gedurende de wedstrijd importeer je de diverse uitslagen. Dit zijn
steeds Excellijstjes met netto-tijden. De deelnemers worden in deze
lijst geïdentificeerd met chipnummer of met teamnummer (startnummer).
Dat kun je op het moment van import zelf kiezen.

De lijsten in Excel hoeven niet gesorteerd te zijn. Je kunt ook meerdere
sublijstjes achter elkaar importeren. Elke import vult aan voor zover
nog niet beschikbaar, en overschrijft eventueel aanwezige uitslagen. Er
is een aparte knop om alle uitslagen voor één onderdeel leeg te maken.

Voor roeien is er een aparte manier van importeren zodat je een lijst
met starttijden (van een stopwatch) en een lijst met eindtijden (van een
andere gesynchroniseerde stopwatch) kunt importeren. Het systeem maakt
dan de koppeling en de berekening van de netto-tijd. Het koppelen van de
lijst met startnummer/starttijd en de lijst met startnummer/eindtijd in
Excel is namelijk soms nog best lastig. (Niet actief in de afgelopen
jaren.)

Het uploaden van een Exceldocument en het importeren van de data en
berekenen van de scores gaat in drie stappen.

1.  Eerst uploaden,

2.  dan via een aparte handeling importeren in de applicatie en

3.  vervolgens scores berekenen.

Tips
----

-   De irritante statusmeldingen bovenin het scherm kun je wegkrijgen
    door erop te klikken.

-   Als je bent ingelogd heb je twee uitslagen-overzichten. De
    *voorlopige* uitslagen zitten onder "Goedkeuring" en de
    *gepubliceerde* uitslagen kun je vinden door op het logo van de Paul
    Veenemans Prijs te klikken. Elk overzicht heeft een eigen
    Excel-download.

-   Bij "Deelnemers" kun je filteren op rugnummer, chipnummer, naam van
    de deelnemer en vereniging. Sorteren door op kolomkop te klikken.

-   Racegroepen kun je bewerken via link onderaan pagina "Deelnemers".

Publicatie van de uitslagen
---------------------------

De uitslagen kun je steeds gepubliceren tot zover de onderdelen voltooid
zijn. Daarvoor is een aparte publicatieknop. De uitslagenlijst onder het
menu-item "Goedkeuring" kun je gebruiken om te tonen aan het Hoofd van
de Jury. Je kunt deze optie eventueel ook gebruiken om de uitslag geheim
te houden tot aan de prijsuitreiking.

Als je iets stoms gedaan hebt en je komt erachter nadat je de uitslagen
gepubliceerd hebt, dan kun je teruggaan naar de vorige versie van de
gepubliceerde uitslagen. Deze "Undo" van gepubliceerde uitslagen werkt
echter maar één niveau diep.

Toegang
-------

De applicatie draait op de publiek toegankelijke URL:
https://app2.paulveenemansprijs.nl/.

Inloggen: `zwolsche` `*****`

Wat zit er niet in
------------------

Je kunt geen realtime tijden publiceren met het systeem. Alles gaat
steeds met een Excel-import.

Ronde-tijden van het schaatsen en andere tussentijden kunnen ook niet in
het PVP-systeem.

Technische achtergrond systeem en back-up
-----------------------------------------

Het PVP-systeem is een speciaal geprogrammeerde applicatie. Het is
gemaakt in C\# en ASP.NET MVC5. Ik heb de source-code beschikbaar indien
gewenst. De mediaan-berekening en de tijdcorrectie voor het roeien in de
tweezonder zijn met uitgebreide unit-tests getest. De berekening is "in
productie" gebruikt bij de PVP sinds 2011.

Het hele systeem draait via een internetverbinding op een server die in
een datacentrum staat. De afgelopen drie jaar was de beschikbaarheid van
deze server 100%, maar hij kan uitvallen. Ik zal daarom zorgen voor een
gesynchroniseerde "hot standby" van deze server zodat jullie bij uitval
verder kunnen werken vanaf die server. Eventuele uitval van
internetverbindingen zullen jullie zelf moeten afdekken door een dongle
(of een "hotspot-telefoon") bij de hand te hebben.

Praktische organisatie en testen
--------------------------------

Ik hoop dat je zo voldoende achtergrondinformatie hebt. Zodra jullie een
draaiboek hebben voor de tijdwaarneming dan kun je me dat misschien
toesturen, dan kan ik kijken of jullie processen aansluiten op het
PVP-systeem.
