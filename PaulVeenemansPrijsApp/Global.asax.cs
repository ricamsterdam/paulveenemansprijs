﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using PaulVeenemansPrijsApp.SiteLogic;

namespace PaulVeenemansPrijsApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.IsLocal || Request.IsSecureConnection || RequestIsWellKnown())
            {
                return;
            }

            Response.RedirectPermanent(ChangeUrlToHttps(Request.Url), true);
        }

        protected void Application_Start()
        {
            GemBoxComponentLicenser.SetValidKey();

            MvcHandler.DisableMvcResponseHeader = true;
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //new MtkSessionFactory(new DatabaseInitializer()).ExposeConfiguration(cfg => new SchemaExport(cfg).SetOutputFile(@"D:\paulveenemansprijs.sql").Create(true, false));
        }

        private static string ChangeUrlToHttps(Uri httpUrl)
        {
            return new UriBuilder(httpUrl) { Scheme = Uri.UriSchemeHttps, Port = -1 }.Uri.ToString();
        }

        private bool RequestIsWellKnown()
        {
            return Request.Path.StartsWith("/.well-known");
        }
    }
}