﻿using System.Collections.Generic;
using PaulVeenemansPrijsApp.Areas.Admin.Models;

namespace PaulVeenemansPrijsApp.Models
{
    public class ResultViewModel
    {
        public IEnumerable<RaceGroupViewModel> RaceGroups { get; set; }
    }
}