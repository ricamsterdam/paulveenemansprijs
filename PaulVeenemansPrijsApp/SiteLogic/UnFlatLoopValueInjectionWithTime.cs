﻿using System.ComponentModel;
using System.Linq;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.BusinessLogic;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public class UnflatLoopValueInjectionWithTime : LoopValueInjectionBase
    {
        protected override void Inject(object source, object target)
        {
            foreach (PropertyDescriptor s in source.GetProps())
            {
                if (s.Name.Contains("Status") || s.Name.Contains("RaceGroup"))
                {
                    continue;
                }
                PropertyDescriptor s1 = s;
                PropertyWithComponent[] endpoints =
                    UberFlatter.Unflat(s.Name, target, type => type == s1.PropertyType || (type == typeof (int?) && s1.PropertyType == typeof (string)))
                               .ToArray();
                if (endpoints.Length == 0)
                {
                    continue;
                }

                object value = s.GetValue(source);

                if (!AllowSetValue(value))
                {
                    continue;
                }
                foreach (PropertyWithComponent endpoint in endpoints)
                {
                    if (endpoint.Property.PropertyType == typeof (int?) && s.PropertyType == typeof (string))
                    {
                        int? i = ((string)value).FromTimeToInt();
                        // ReSharper disable AssignNullToNotNullAttribute
                        endpoint.Property.SetValue(endpoint.Component, i);
                    }
                    else
                    {
                        // ReSharper disable AssignNullToNotNullAttribute
                        endpoint.Property.SetValue(endpoint.Component, value);
                    }
                }
            }
        }
    }
}