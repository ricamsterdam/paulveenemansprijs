﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public class ExactlyOneFieldRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string[] _fields;

        public ExactlyOneFieldRequiredAttribute(params string[] fields)
        {
            _fields = fields;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule {ErrorMessage = ErrorMessage, ValidationType = "multifield"};
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int fieldCount = 0;
            foreach (string field in _fields)
            {
                PropertyInfo property = validationContext.ObjectType.GetProperty(field);
                if (property == null)
                {
                    return new ValidationResult($"Property '{field}' is undefined.");
                }

                object fieldValue = property.GetValue(validationContext.ObjectInstance, null);

                if (fieldValue != null && !string.IsNullOrEmpty(fieldValue.ToString()))
                {
                    fieldCount++;
                }
            }
            return fieldCount != 1 ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName), _fields) : null;
        }
    }
}