﻿using System.IO;
using GemBox.Spreadsheet;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class GemBoxOptionsRetriever
    {
        public static LoadOptions GetLoadOptions(string fileName)
        {
            return Path.GetExtension(fileName)?.ToLowerInvariant().EndsWith("xls") == true ? (LoadOptions)LoadOptions.XlsDefault : LoadOptions.XlsxDefault;
        }
    }
}
