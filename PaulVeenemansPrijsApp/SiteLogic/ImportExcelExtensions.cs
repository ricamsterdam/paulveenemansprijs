﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using GemBox.Spreadsheet;
using PaulVeenemansPrijsDomain.BusinessLogic;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class ImportExcelExtensions
    {
        public static int GetSheetsCount(this byte[] data, LoadOptions loadOptions)
        {
            ExcelFile book = ExcelFile.Load(new MemoryStream(data), loadOptions);
            return book.Worksheets.Count(worksheet => worksheet.Rows.Count > 1);
        }

        public static void Import(this byte[] data, LoadOptions loadOptions, IParticipantRepository participantRepository,
            IRaceGroupRepository raceGroupRepository, IStatusRepository statusRepository)
        {
            ExcelFile book = ExcelFile.Load(new MemoryStream(data), loadOptions);
            ExcelWorksheet sheet = book.Worksheets[0];
            for (var i = 1; i < sheet.Rows.Count; i++)
            {
                ExcelRow excelRow = sheet.Rows[i];
                if (excelRow.Cells[0].Value == null)
                {
                    continue;
                }

                var j = 0;
                Participant participant;
                try
                {
                    participant = new Participant
                    {
                        TeamSeqNumber = excelRow.Cells[j++].Value.FromObjectToInt(),
                        ChipNumber = excelRow.Cells[j++].Value.FromObjectToString(),
                        RaceCodeMajor = excelRow.Cells[j++].Value.FromObjectToString(),
                        RaceCodeMinor = excelRow.Cells[j++].Value.FromObjectToString(),
                        TeamCode = excelRow.Cells[j++].Value.FromObjectToString(),
                        ParticipantName = excelRow.Cells[j++].Value.FromObjectToString(),
                        CodeRowing = CleanCodeRowing(excelRow.Cells[j++].Value.FromObjectToString()),
                        RaceGroup = raceGroupRepository.Proxy(1),
                        Skating = new Part { Status = statusRepository.Proxy(1) },
                        Running = new Part { Status = statusRepository.Proxy(1) },
                        Cycling = new Part { Status = statusRepository.Proxy(1) },
                        Rowing = new RowingPart { Status = statusRepository.Proxy(1) }
                    };
                    var raceGroupString = excelRow.Cells[j].Value.FromObjectToString().Trim();
                    if (!string.IsNullOrEmpty(raceGroupString))
                    {
                        RaceGroup raceGroupEntity = raceGroupRepository.FindBy(x => x.RaceGroupDescription.Contains(raceGroupString));
                        if (raceGroupEntity != null)
                        {
                            participant.RaceGroup = raceGroupEntity;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException($"Error in import; row {i} column {j} message {e.Message}", e);
                }

                participantRepository.Add(participant);
                participantRepository.Save();
            }

            participantRepository.DisposeSession();
        }

        public static void Import(this ImportExcel importExcel, IParticipantRepository participantRepository)
        {
            var participants = participantRepository.All();
            ExcelFile book = ExcelFile.Load(new MemoryStream(importExcel.ExcelData),
                GemBoxOptionsRetriever.GetLoadOptions(importExcel.ExcelFileName));
            ExcelWorksheet sheet = book.Worksheets[0];
            var teamNumberColumn = importExcel.TeamNumberColumn.GetValueOrDefault() - 1;
            var chipNumberColumn = importExcel.ChipNumberColumn.GetValueOrDefault(1) - 1;

            for (var i = importExcel.SkipRows; i < sheet.Rows.Count; i++)
            {
                Participant participant = null;
                if (teamNumberColumn >= 0)
                {
                    var teamSeqNumber = sheet.Cells[i, teamNumberColumn].Value.FromObjectToInt();
                    if (teamSeqNumber > 0)
                    {
                        participant = participants.FirstOrDefault(p => p.TeamSeqNumber == teamSeqNumber);
                    }
                }

                if (participant == null)
                {
                    var excelValue1 = sheet.Cells[i, chipNumberColumn].Value.FromObjectToString();
                    var excelValue2 = teamNumberColumn >= 0 ? sheet.Cells[i, teamNumberColumn].Value.FromObjectToString() : excelValue1;
                    participant = participants.FirstOrDefault(p => p.ChipNumber == excelValue1 || p.ChipNumber == excelValue2);
                }

                var timeCellValueObj = sheet.Cells[i, importExcel.TimeColumn - 1].Value;
                if (participant == null || timeCellValueObj == null)
                {
                    continue;
                }

                if (!(timeCellValueObj is DateTime excelValueTime))
                {
                    if (sheet.Cells[i, importExcel.TimeColumn - 1].ValueType != CellValueType.String ||
                        !Regex.IsMatch(sheet.Cells[i, importExcel.TimeColumn - 1].Style.NumberFormat, "h:m+:s+.0+", RegexOptions.IgnoreCase))
                    {
                        continue;
                    }

                    // Special case where the cell is formatted as time, but the underlying cell value is still text. Appears in RaceClocker export.
                    var s = timeCellValueObj.ToString().Trim();
                    var format = Regex.IsMatch(s, @"\d{2}$") ? "h:m:s.ff" : "h:m:s.f";
                    if (DateTime.TryParseExact(s, format, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out DateTime dt))
                    {
                        excelValueTime = dt;
                    }
                    else
                    {
                        continue;
                    }
                }

                var time = (int)excelValueTime.TimeOfDay.TotalMilliseconds;
                if (time < 1000 || time > 10000000 /* Higher value is not realistic: > 02h 47m */)
                {
                    continue;
                }

                if (importExcel.Race == Parts.Cycling)
                {
                    participant.Cycling.Time = time + (participant.Cycling.Penalty ?? 0);
                }
                else if (importExcel.Race == Parts.Rowing)
                {
                    participant.Rowing.Time = time + (participant.Rowing.Penalty ?? 0);
                    participant.Rowing.TimeCorrected = (int)(GetCorrectionFactor(participant.CodeRowing) * (double)participant.Rowing.Time);
                }
                else if (importExcel.Race == Parts.Running)
                {
                    participant.Running.Time = time + (participant.Running.Penalty ?? 0);
                }
                else if (importExcel.Race == Parts.Skating)
                {
                    participant.Skating.Time = time + (participant.Skating.Penalty ?? 0);
                }
            }
        }

        private static string CleanCodeRowing(string codeRowing)
        {
            var code = codeRowing.Trim();
            switch (code)
            {
                case "1x":
                    return "1x";
                case "2x":
                    return "2x";
                case "2-":
                    return "2-";
                default:
                    throw new ArgumentOutOfRangeException(nameof(codeRowing), codeRowing, "CodeRowing out of range. Must be 1x, 2x or 2-.");
            }
        }

        private static double GetCorrectionFactor(string codeRowing)
        {
            switch (codeRowing)
            {
                case "1x":
                    return 1;
                case "2x":
                    return 1.11;
                case "2-":
                    return 1.055;
                default:
                    throw new ArgumentOutOfRangeException(nameof(codeRowing), codeRowing, "CodeRowing out of range. Must be 1x, 2x or 2-.");
            }
        }
    }
}