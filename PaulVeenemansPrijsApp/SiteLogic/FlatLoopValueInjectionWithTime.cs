﻿using System.ComponentModel;
using System.Linq;
using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.BusinessLogic;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public class FlatLoopValueInjectionWithTime : LoopValueInjectionBase
    {
        protected override void Inject(object source, object target)
        {
            foreach (PropertyDescriptor t in target.GetProps())
            {
                PropertyDescriptor t1 = t;

                PropertyWithComponent endpoint =
                    UberFlatter.Flat(t.Name, source, type => t1.PropertyType == type || (t1.PropertyType == typeof (string) && type == typeof (int?)))
                               .FirstOrDefault();
                if (endpoint == null)
                {
                    continue;
                }
                object val = endpoint.Property.GetValue(endpoint.Component);

                if (!AllowSetValue(val))
                {
                    continue;
                }
                if (t.PropertyType == typeof (string) && endpoint.Property.PropertyType == typeof (int?))
                {
                    t.SetValue(target, ((int?)val).FromIntToTime());
                }
                else
                {
                    // ReSharper disable AssignNullToNotNullAttribute
                    t.SetValue(target, val);
                }
            }
        }
    }
}