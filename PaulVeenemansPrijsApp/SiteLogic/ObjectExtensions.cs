﻿using System.Text.RegularExpressions;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class ObjectExtensions
    {
        public static int FromObjectToInt(this object value)
        {
            return value != null && Regex.IsMatch(value.ToString().Trim(), @"^\d+$") ? int.Parse(value.ToString().Trim()) : 0;
        }

        public static string FromObjectToString(this object value)
        {
            return value != null ? value.ToString().Trim() : "";
        }
    }
}