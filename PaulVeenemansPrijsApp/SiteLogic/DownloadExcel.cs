﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GemBox.Spreadsheet;
using PaulVeenemansPrijsDomain.BusinessLogic;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public class DownloadExcel
    {
        private readonly IRaceGroupRepository _raceGroupRepository;

        public DownloadExcel(IRaceGroupRepository raceGroupRepository)
        {
            _raceGroupRepository = raceGroupRepository;
        }

        public byte[] GetBytes(Func<RaceGroup, IEnumerable<IParticipant>> getParticipantsFunc)
        {
            var book = new ExcelFile();
            ExcelWorksheet sheet = book.Worksheets.Add(GetSheetName());
            var rowIndex = 0;
            var colIndex = 0;
            sheet.Cells[rowIndex, colIndex].Row.Style.Font.Weight = ExcelFont.BoldWeight;
            sheet.Cells[rowIndex, colIndex++].Value = "#";
            sheet.Cells[rowIndex, colIndex++].Value = "Startnr.";
            sheet.Cells[rowIndex, colIndex++].Value = "Cat.";
            sheet.Cells[rowIndex, colIndex++].Value = "Vet.";
            sheet.Cells[rowIndex, colIndex++].Value = "Ploegnaam";
            sheet.Cells[rowIndex, colIndex++].Value = "Deelnemer";
            sheet.Cells[rowIndex, colIndex++].Value = "R";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "mm:ss.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Wielrennen tijd";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "0.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Wielrennen pt.";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "mm:ss.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Schaatsen tijd";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "0.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Schaatsen pt.";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "mm:ss.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Hardlopen tijd";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "0.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Hardlopen pt.";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "mm:ss.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Roeien tijd";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "mm:ss.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Roeien 2-";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "0.0";
            sheet.Cells[rowIndex, colIndex++].Value = "Roeien pt.";
            sheet.Cells[rowIndex, colIndex].Column.Style.NumberFormat = "0.000";
            sheet.Cells[rowIndex++, colIndex].Value = "Totaal pt.";
            foreach (RaceGroup raceGroup in _raceGroupRepository.All())
            {
                sheet.Cells[++rowIndex, 1].Style.Font.Italic = true;
                sheet.Cells[rowIndex++, 1].Value = raceGroup.RaceGroupDescription;
                var rank = 1;
                IEnumerable<IParticipant> participants = getParticipantsFunc(raceGroup);
                foreach (IParticipant participant in participants.OrderByDescending(p => p.OverallScore))
                {
                    colIndex = 0;
                    sheet.Cells[rowIndex, colIndex++].Value = rank++;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.TeamSeqNumber;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.RaceCodeMajor;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.RaceCodeMinor;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.TeamCode;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.ParticipantName;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.CodeRowing;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Cycling.Time.FromIntToDateTime();
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Cycling.Score;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Skating.Time.FromIntToDateTime();
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Skating.Score;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Running.Time.FromIntToDateTime();
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Running.Score;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Rowing.Time.FromIntToDateTime();
                    if (participant.Rowing.Time != participant.Rowing.TimeCorrected)
                    {
                        sheet.Cells[rowIndex, colIndex].Value = participant.Rowing.TimeCorrected.FromIntToDateTime();
                    }

                    colIndex++;
                    sheet.Cells[rowIndex, colIndex++].Value = participant.Rowing.Score;
                    sheet.Cells[rowIndex++, colIndex].Value = participant.OverallScore;
                }
            }

            using (var ms = new MemoryStream())
            {
                book.Save(ms, SaveOptions.XlsDefault);
                return ms.GetBuffer();
            }
        }

        private string GetSheetName()
        {
            string sheetName = _raceGroupRepository.FindBy(1).RaceGroupDescription.Replace(" ", "");
            return sheetName.Length > 30 ? sheetName.Substring(0, 30) : sheetName;
        }
    }
}
