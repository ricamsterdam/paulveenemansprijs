﻿#define PROFESSIONAL_LICENSE
using GemBox.Spreadsheet;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class GemBoxComponentLicenser
    {
        private const string GemBoxSpreadsheetProfessionalLicenseKey = "SN-2019Oct04-Ieh8ViMiEwLNOwCqV8seW7juH4bh2Zz818a0VhfDRthnNhr3v5w8C89R5UtAOLWqxKjRX/4CbbZMaamvrq8xCnc3H1A==A"; // Valid for version 4.5, purchase date 04/10/2019

        public static void SetValidKey()
        {
#if PROFESSIONAL_LICENSE
            SpreadsheetInfo.SetLicense(GemBoxSpreadsheetProfessionalLicenseKey);
#else
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
#endif
        }
    }
}
