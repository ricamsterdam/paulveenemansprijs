﻿using Omu.ValueInjecter;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsApp.Areas.Admin.Models;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class ParticipantExtensions
    {
        public static ParticipantViewModel MapToNewParticipantViewModel(this IParticipant participant)
        {
            var participantViewModel = new ParticipantViewModel();
            participantViewModel.InjectFrom<FlatLoopValueInjectionWithTime>(participant);
            return participantViewModel;
        }

        public static ParticipantViewModel MapToNewParticipantViewModel(this IParticipant participant, ref int index)
        {
            ParticipantViewModel participantViewModel = participant.MapToNewParticipantViewModel();
            participantViewModel.Index = index++;
            return participantViewModel;
        }
    }
}