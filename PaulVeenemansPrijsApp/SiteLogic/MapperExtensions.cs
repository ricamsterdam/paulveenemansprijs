﻿using Omu.ValueInjecter;

namespace PaulVeenemansPrijsApp.SiteLogic
{
    public static class MapperExtensions
    {
        public static TResult MapToNew<TResult>(this object source) where TResult : new()
        {
            var result = new TResult();
            result.InjectFrom(source);
            return result;
        }

        public static TResult MapToNew<TResult, TValueInjection>(this object source) where TResult : new() where TValueInjection : IValueInjection, new()
        {
            var result = new TResult();
            result.InjectFrom<TValueInjection>(source);
            return result;
        }
    }
}
