﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using PaulVeenemansPrijsApp.Areas.Admin.Models;
using PaulVeenemansPrijsApp.Models;
using PaulVeenemansPrijsApp.SiteLogic;
using PaulVeenemansPrijsDomain.BusinessLogic;
using PaulVeenemansPrijsDomain.Entities;
using PaulVeenemansPrijsDomain.Repositories;
using WebMarkupMin.Core;

namespace PaulVeenemansPrijsApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRaceGroupRepository _raceGroupRepository;
        private readonly IYearRepository _yearRepository;

        public HomeController(IRaceGroupRepository raceGroupRepository, IYearRepository yearRepository)
        {
            _raceGroupRepository = raceGroupRepository;
            _yearRepository = yearRepository;
        }

        public ActionResult Excel(bool anonymous = false)
        {
            var prefix = Request.IsAuthenticated && !anonymous ? "gepubliceerde-scores" : "paulveenemansprijs";
            return File(new DownloadExcel(_raceGroupRepository).GetBytes(rg => rg.ParticipantsApproved),
                "application/vnd.excel",
                $"{prefix}{_yearRepository.CurrentYear().Name}.xls");
        }

        [Authorize]
        public ActionResult HtmlExport()
        {
            var raceGroupViewModels = GetRaceGroupViewModels();

            ViewBag.PdfFileName = ConfigurationManager.AppSettings["PdfFileName"];
            ViewBag.Title = $"Uitslagen Paul Veenemansprijs {_yearRepository.CurrentYear().Name}";

            var html = this.RenderRazorViewToString("HtmlExport", new ResultViewModel { RaceGroups = raceGroupViewModels });
            MarkupMinificationResult result = new HtmlMinifier().Minify(html);
            return File(Encoding.UTF8.GetBytes(result.MinifiedContent), "text/html", "html-export.html");
        }

        public ActionResult Index(bool anonymous = false)
        {
            var raceGroupViewModels = GetRaceGroupViewModels();

            ViewBag.PdfFileName = ConfigurationManager.AppSettings["PdfFileName"];
            ViewBag.Title = $"Uitslagen Paul Veenemansprijs {_yearRepository.CurrentYear().Name}";
            ViewBag.IsAnonymous = anonymous;
            return View(new ResultViewModel { RaceGroups = raceGroupViewModels });
        }

        public ActionResult VrouwenMannen()
        {
            var raceGroupViewModels = GetRaceGroupViewModels();

            ViewBag.Title = $"Uitslagen Paul Veenemansprijs {_yearRepository.CurrentYear().Name}";
            return View(new ResultViewModel { RaceGroups = raceGroupViewModels });
        }

        private IEnumerable<RaceGroupViewModel> GetRaceGroupViewModels()
        {
            var raceGroupViewModels = new List<RaceGroupViewModel>();
            foreach (RaceGroup raceGroup in _raceGroupRepository.All().OrderBy(r => r.RaceGroupId))
            {
                var raceGroupViewModel = raceGroup.MapToNew<RaceGroupViewModel>();
                var i = 1;
                raceGroupViewModel.Participants = raceGroup.ParticipantsApproved.OrderByDescending(p => p.OverallScore)
                    .Select(x => x.MapToNewParticipantViewModel(ref i))
                    .ToList();
                raceGroupViewModel.Remarks = raceGroup.Remarks.Select(x => x.MapToNew<RemarkViewModel>()).ToList();
                raceGroupViewModels.Add(raceGroupViewModel);
            }

            return raceGroupViewModels;
        }
    }
}