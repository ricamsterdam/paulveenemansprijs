﻿var replaceImagesOnElements;

$(function() {

    var test = new Image();
    test.src = $.mtk.content + 'icons/add.png?' + new Date().getTime();
    test.onload = function() {

        replaceImagesOnElements = function(elements) {
            for (var i in elements) {
                var classNames = elements[i].className.split( /\s+/ );
                for (var j in classNames) {
                    if (classNames[j].match('\.png$')) {
                        var newChild = document.createElement('img');
                        newChild.src = $.mtk.content + 'icons/' + classNames[j];
                        newChild.alt = newChild.title = elements[i].firstChild.nodeValue;
                        elements[i].replaceChild(newChild, elements[i].firstChild);
                    }
                }
            }
            $('#loadmask').fadeOut(100, function() { $('#loadmask').remove(); });
        };
        replaceImagesOnElements($('a, span').get());

    };
});