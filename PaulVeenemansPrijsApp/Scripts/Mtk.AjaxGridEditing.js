﻿$(function() {

    $('body').append('<div id="dialog" style="display: none;"></div>');

    function saveForm(form, link, isCreateAction, callBackOnSuccess) {
        if (!form.valid()) {
            return;
        }
        form.ajaxSubmit({
            type: isCreateAction ? 'POST' : 'PUT',
            success: function(data) {
                if (new RegExp('<h2>').test(data)) {
                    $('#dialog').html(data);
                    $('#dialog h2,#dialog p').remove();
                } else {
                    var row;
                    if (isCreateAction) {
                        $('tr.gridrow-empty').remove();
                        $('tbody:eq(0)').append($(data).find('tr'));
                        row = $('tbody:eq(0) tr:last');
                    } else {
                        row = $(link.parents('tr:eq(0)'));
                        row.html($(data).find('td'));
                    }
                    if (replaceImagesOnElements) {
                        replaceImagesOnElements(row.find('a, span').get());
                    }
                    $('h2').after($(data).find('.status-message'));
                    callBackOnSuccess();
                }
            }
        });
    }

    function createHandler(link) {
        $('.status-message').remove();
        var myDialog = $('#dialog');
        myDialog.load(link.attr('href'), function() {
            var title = $('h2', myDialog).html();
            $('h2, p', myDialog).remove();
            myDialog.dialog({
                buttons: {
                    'Opslaan en nieuw': function() {
                        saveForm($('form', myDialog), link, true, function() {
                            createHandler(link);
                        });
                    },
                    Annuleren: function() {
                        myDialog.dialog('close');
                    },
                    Opslaan: function() {
                        saveForm($('form', myDialog), link, true, function() {
                            myDialog.dialog('close');
                        });
                    }
                },
                height: 'auto',
                modal: true,
                open: function() {
                    $(':button:contains(Opslaan)').focus();
                },
                resizable: false,
                title: title,
                width: 'auto'
            });
            $.validator.unobtrusive.parse(this);
        });
    }

    function editHandler(link) {
        $('.status-message').remove();
        var myDialog = $('#dialog');
        myDialog.load(link.attr('href'), function() {
            var title = $('h2', myDialog).html();
            $('h2, p', myDialog).remove();
            myDialog.dialog({
                buttons: {
                    '« Opslaan': function() {
                        var prev = link.parents('tr:eq(0)').prev().find('a.Edit');
                        saveForm($('form', myDialog), link, false, function () {
                            if (prev.length > 0) {
                                editHandler(prev);
                            } else {
                                myDialog.dialog('close');
                            }
                        });
                    },
                    'Opslaan »': function() {
                        var next = link.parents('tr:eq(0)').next().find('a.Edit');
                        saveForm($('form', myDialog), link, false, function () {
                            if (next.length > 0) {
                                editHandler(next);
                            } else {
                                myDialog.dialog('close');
                            }
                        });
                    },
                    Annuleren: function() {
                        myDialog.dialog('close');
                    },
                    Opslaan: function() {
                        saveForm($('form', myDialog), link, false, function() {
                            myDialog.dialog('close');
                        });
                    }
                },
                height: 'auto',
                maxHeight: 640,
                maxWidth: 900,
                modal: true,
                open: function() {
                    $(':button:contains(Opslaan)').last().focus();
                },
                resizable: false,
                title: title,
                width: 'auto'
            });
            $.validator.unobtrusive.parse(this);
        });
    }

    function detailsHandler(link) {
        $('.status-message').remove();
        var myDialog = $('#dialog');
        myDialog.load(link.attr('href'), function() {
            var title = $('h2', myDialog).html();
            $('h2, p', myDialog).remove();
            myDialog.dialog({
                buttons: {
                    '« Vorige': function() {
                        var prev = link.parents('tr:eq(0)').prev().find('a.Details');
                        if (prev.length > 0) {
                            detailsHandler(prev);
                        } else {
                            myDialog.dialog('close');
                        }
                    },
                    'Volgende »': function() {
                        var next = link.parents('tr:eq(0)').next().find('a.Details');
                        if (next.length > 0) {
                            detailsHandler(next);
                        } else {
                            myDialog.dialog('close');
                        }
                    },
                    Bewerken: function() {
                        myDialog.dialog('close');
                        editHandler(link.next(':eq(0)'), false);
                    },
                    Sluiten: function() {
                        myDialog.dialog('close');
                    }
                },
                height: 'auto',
                maxHeight: 640,
                maxWidth: 900,
                modal: true,
                open: function() {
                    $(':button:contains(Sluiten)').focus();
                },
                resizable: false,
                title: title,
                width: 'auto'
            });
        });
    }

    function deleteHandler(link) {
        $('.status-message').remove();
        var recordText = link.parents('tr:eq(0)').find('td:eq(0)').html();
        var myDialog = $('#dialog');
        myDialog.html($.validator.format('<p>Weet je zeker dat je {0} "{1}" wilt verwijderen?</p>', $.mtk.entityName.toLowerCase(), $.trim(recordText)));
        myDialog.dialog({
            buttons: {
                Annuleren: function() {
                    myDialog.dialog('close');
                },
                Verwijderen: function() {
                    $.ajax({
                        url: link.attr('href'),
                        type: 'DELETE',
                        success: function(data) {
                            if (!$(data).find('.error').length) {
                                link.parents('tr:eq(0)').remove();
                            }
                            $('h2').after($(data).find('.status-message'));
                        }
                    });
                    myDialog.dialog('close');
                }
            },
            height: 'auto',
            maxHeight: 640,
            maxWidth: 900,
            modal: true,
            open: function() {
                $(':button:contains(Annuleren)').focus();
            },
            resizable: false,
            title: $.validator.format('{0} verwijderen', $.mtk.entityName),
            width: 'auto'
        });
    }

    $('.Create').on('click', function() {
        createHandler($(this));
        return false;
    });

    $('table.grid-editing').on('click', '.Edit', function() {
        editHandler($(this));
        return false;
    });

    $('table.grid-editing').on('click', '.Details', function() {
        detailsHandler($(this));
        return false;
    });

    $('table.grid-editing ').on('click', '.Delete', function() {
        deleteHandler($(this));
        return false;
    });
});