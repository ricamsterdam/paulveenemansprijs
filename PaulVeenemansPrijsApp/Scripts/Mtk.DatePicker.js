﻿$(function () {
    $.datepicker.setDefaults({
        buttonImage: $.mtk.content + 'icons/datepicker.png',
        changeMonth: true,
        changeYear: true,
        closeText: 'Sluiten',
        currentText: 'Vandaag',
        dateFormat: "dd-mm-yy",
        dayNames: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
        dayNamesMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
        dayNamesShort: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
        firstDay: 1,
        minDate: '01-01-2011',
        monthNames: ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
        monthNamesShort: ['jan', 'feb', 'maa', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
        showMonthAfterYear: false,
        showOn: 'button',
        weekHeader: 'Wk',
        yearRange: '2012:2020'
    });
    $('.date-picker').datepicker();
});