﻿using System.Web.Mvc;
using System.Web.Routing;
using LowercaseRoutesMVC;

namespace PaulVeenemansPrijsApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("sitemap.xml");
            routes.IgnoreRoute("robots.txt");
            routes.IgnoreRoute("favicon.ico");

            routes.MapRouteLowercase("Default", "{controller}/{action}/{id}", new {controller = "Home", action = "Index", id = UrlParameter.Optional});
            routes.MapRouteLowercase("Root", "", new {controller = "Home", action = "Index", id = ""});
        }
    }
}