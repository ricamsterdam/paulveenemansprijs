using HibernatingRhinos.Profiler.Appender.NHibernate;
using PaulVeenemansPrijsApp;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(NHibernateProfilerBootstrapper), "PreStart")]

namespace PaulVeenemansPrijsApp
{
    public static class NHibernateProfilerBootstrapper
    {
        public static void PreStart()
        {
            NHibernateProfiler.Initialize();
        }
    }
}