using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Mtk.Domain.NHibernate;
using Ninject;
using Ninject.Web.Common;
using PaulVeenemansPrijsDomain.NHibernate;
using PaulVeenemansPrijsDomain.Repositories;
using PaulVeenemansPrijsApp;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace PaulVeenemansPrijsApp
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>().InSingletonScope();
            kernel.Bind<IMtkSessionFactory>().To<MtkSessionFactory>().InSingletonScope();
            kernel.Bind<IMtkTransactionProvider>().To<MtkTransactionProvider>().InRequestScope();
            kernel.Bind<IParticipantRepository>().To<ParticipantRepository>();
            kernel.Bind<IParticipantApprovedRepository>().To<ParticipantApprovedRepository>();
            kernel.Bind<IParticipantArchivedRepository>().To<ParticipantArchivedRepository>();
            kernel.Bind<IParticipantExcelRepository>().To<ParticipantExcelRepository>();
            kernel.Bind<IRaceGroupRepository>().To<RaceGroupRepository>();
            kernel.Bind<IRemarkRepository>().To<RemarkRepository>();
            kernel.Bind<ICalculationReportRepository>().To<CalculationReportRepository>();
            kernel.Bind<IStatusRepository>().To<StatusRepository>();
            kernel.Bind<ILoginRepository>().To<LoginRepository>();
            kernel.Bind<IYearRepository>().To<YearRepository>();
        }
    }
}
